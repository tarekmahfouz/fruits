<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'weight_unit' => $this->product_weight_unit,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'total_cost' => $this->total_price,
            'product' => new ProductResource($this->product),
        ];
    }
}
