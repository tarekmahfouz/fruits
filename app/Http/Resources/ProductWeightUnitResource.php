<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductWeightUnitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'weight_id' => $this->unit->id,
            'weight_unit' => $this->unit->name,
            'weight_price' => $this->price,
            'start_from' => $this->start_from
        ];
    }
}
