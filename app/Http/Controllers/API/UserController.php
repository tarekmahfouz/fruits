<?php

namespace App\Http\Controllers\API;


use Carbon\Carbon;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests\CartRequest;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ChackoutRequest;
use App\Http\Resources\CartItemResource;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
	protected $serviceObj;
	protected $user;

    public function __construct(CommonService $serviceObj)
    {
        Carbon::setLocale('ar');
        $this->middleware(['auth:api']);
        $this->user = Auth::guard('api')->user();
    	$this->serviceObj = $serviceObj;
    }

    ##### DONE
    public function me(Request $request)
    {
        $code = 200;
        $message = 'done.';
        $item = null;
        try {
            $item = $this->user;
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $item);
    }

    public function editProfile(UserRequest $request)
    {
        $code = 201;
        $message = 'done.';
        $result = [];
        try {
            $data = $request->except(['image','_method','password_confirmation']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/users', true, $this->user->image);
            }
            if($request->filled('password')) {
                $data['password'] = bcrypt($data['password']);
            }
            $result = $this->serviceObj->update('User', ['id' => $this->user->id], $data);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            }
            $result = $this->serviceObj->find('User', ['id' => $this->user->id]);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $result);
    }

    public function logout(Request $request)
    {
        $code = 200;
        $message = 'logged out successfully.';
        try {
            Auth::guard('api')->logout();
        } catch (\Exception $e) {
            $code = $e->getCode() ? $e->getCode() : 400 ;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }

    public function addToCart(CartRequest $request)
    {
        $code = 201;
        $message = 'done.';
        $cartData = [];
        try {
            # Get Product Info & Price Info
            $product = $this->serviceObj->find('Product' ,['id' => $request->product_id]);
            $productWeightUnit = $this->serviceObj->find('ProductWeightUnit' ,['id' => $request->product_weight_unit_id]);
            
            if($productWeightUnit->product_id != $request->product_id) {
                throw new \Exception("product Weight Unit ID error", 404);
            }
            # Check if there's any not ordered cart 
            $lastCart = $this->serviceObj->find('Cart' ,['user_id' => $this->user->id, 'ordered' => 0]);
            
            if($lastCart) {
                $itemData = [];
                $itemData['cart_id']     = $lastCart->id;
                $itemData['product_id'] = $request->product_id;
                $itemData['product_weight_unit'] = $productWeightUnit->unit->name;
                $itemData['price']   = $productWeightUnit->price;
                $itemData['start_from']   = $productWeightUnit->start_from;
                $itemData['quantity']   = $request->quantity;
                $itemData['total_price']   = $productWeightUnit->price * $request->quantity;
                $this->serviceObj->create('CartItem', $itemData);
            } else {
                $newCartData = [
                    'user_id' => $this->user->id,
                    'ordered' => 0
                ];
                $newCart = $this->serviceObj->create('Cart', $newCartData);
                $itemData = [];
                $itemData['cart_id']     = $newCart->id;
                $itemData['product_id'] = $request->product_id;
                $itemData['product_weight_unit'] = $productWeightUnit->unit->name;
                $itemData['price']   = $productWeightUnit->price;
                $itemData['start_from']   = $productWeightUnit->start_from;
                $itemData['quantity']   = $request->quantity;
                $itemData['total_price']   = $productWeightUnit->price * $request->quantity;
                $this->serviceObj->create('CartItem', $itemData);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, []);
    }
    
    public function cartItems(Request $request)
    {
        $code = 201;
        $message = 'done.';
        $data = [];
        try {
            $totalCost = 0;
            $cart = $this->serviceObj->find('Cart' ,['user_id' => $this->user->id, 'ordered' => 0]);
            
            if($cart) {
                $items = $this->serviceObj->getAll('CartItem' ,['cart_id' => $cart->id]);
                foreach($items as $item) {
                    $totalCost += $item->total_price;
                }
            } else {
                throw new \Exception("لا يوجد منتجات", 400);
            }
            $data = [
                'cart_id'   => $cart->id,
                'totalCost' => number_format($totalCost, 2),
                'items'     => CartItemResource::collection($items)
            ];
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function editCartItem(CartRequest $request)
    {
        $code = 201;
        $message = 'done.';
        try {
            $userID = $this->user->id;
            $cartID = $request->cart_id;
            $cart = $this->serviceObj->find('Cart',['id' => $cartID, 'ordered' => 0]);
            if(!$cart || $cart->user_id != $userID) {
                throw new \Exception('غير موجود!', 404);
            }
            
            $this->serviceObj->destroy('CartItem',['cart_id' => $cartID]);

            foreach($request->items as $item) {
                $item = (object)$item;
                
                $productWeightUnit = $this->serviceObj->find('ProductWeightUnit' ,['id' => $item->product_weight_unit_id]);
                if($productWeightUnit->product_id != $item->product_id) {
                    throw new \Exception("product Weight Unit ID error", 404);
                }
                $data[] = [
                    'cart_id' => $cartID,
                    'product_id' => $item->product_id,
                    'quantity' => $item->quantity,
                    'product_weight_unit' => $productWeightUnit->unit->name,
                    'price' => $productWeightUnit->price,
                    'start_from'  => $productWeightUnit->start_from,
                    'total_price' => $productWeightUnit->price * $item->quantity,
                ];
            }
            $this->serviceObj->bulkInsert('CartItem',$data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode()) ;
            $message = $e->getMessage();//.' - '.$e->getFile().' - '.$e->getLine();
        }
        return jsonResponse($code, $message);
    }

    public function deleteCartItem(Request $request)
    {
        $code = 201;
        $message = 'done.';
        try {
            $userID = $this->user->id;
            $conditions['id'] = $request->item_id;
            $conditions = [
                'id' => $request->cart_item_id,
                'cart_id' => $request->cart_id,
            ];

            $cart = $this->serviceObj->find('Cart',['id' => $request->cart_id, 'ordered' => 0]);
            $cartItem = $this->serviceObj->find('CartItem',$conditions);
            if(!$cart || !$cartItem || $cart->user_id != $userID) {
                throw new \Exception('not existed!', 404);
            }
            $result = $this->serviceObj->destroy('CartItem',$conditions);
            if(!$result)
                throw new \Exception('something went wrong!', 400);
        } catch (\Exception $e) {
            $code = getCode($e->getCode()) ;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }

    public function checkout(ChackoutRequest $request)
    {
        $code = 201;
        $message = 'done.';

        \DB::beginTransaction();
        try {
            $data = $request->only(['lat','lon','address','cart_id','day','payment_way','notes']);
            $userID = $data['user_id'] = $this->user->id;
            $conditions = [
                'id' => $request->cart_id,
                'user_id' => $this->user->id,
                'ordered' => 0
            ];
            $cart = $this->serviceObj->find('Cart', $conditions);
            
            $availability = $this->serviceObj->find('Availability', ['id' => $request->availability_id]);
            $config = $this->serviceObj->find('Config');

            if(!$cart) {
                throw new \Exception('not existed!', 404);
            }
            if(!in_array($request->payment_way, ['cach', 'bank'])) {
                throw new \Exception('Payment type must be cach or bank!', 400);
            }
            
            $totalCost = 0;
            $items = $this->serviceObj->getAll('CartItem' ,['cart_id' => $cart->id], ['product']);
            foreach($items as $item) {
                $totalCost += $item->total_price;
            }
            
            $data['shipping_value'] = $config->shipping_value ?? 0 ;
            
            $data['time_from'] = $availability->time_from;
            $data['time_to'] = $availability->time_to;
            
            if($request->filled('promo_code')) {
                $promoCode = $this->serviceObj->find('PromoCode', ['code' => $request->promo_code]);
                $data['promo_code'] = $promoCode->code;
                $data['promo_code_value'] = $promoCode->value;
            } else {
                $data['promo_code'] = null;
                $data['promo_code_value'] = 0;
            }
            $data['user_id'] = $this->user->id;
            $data['total_cost'] = ($totalCost - $data['promo_code_value']) + $data['shipping_value'] ;
            $data['status'] = 'preview';
            
            $result = $this->serviceObj->create('Order', $data);
            $this->serviceObj->update('Cart', $conditions, ['ordered' => 1]);
            
            if(!$result)
                throw new \Exception('something went wrong!', 400);

            /* ================================================== */
            $orderData['order'] = $result;
            $from = env('MAIL_USERNAME');
            Mail::send('mails.order', $orderData, function ($mail) use ($orderData, $from) {
                $mail->from($from, 'Fruits App');
                $mail->to('fruitsapps2020@gmail.com', 'Fruits App')->subject('Fruits App Syatem');
            });
            /* ================================================== */

            \DB::commit();
        } catch (\Exception $e) {
            $code = getCode($e->getCode()) ;
            $message = $e->getMessage().' '.$e->getLine();
            \DB::rollback();
        }
        return jsonResponse($code, $message);
    }

    public function myOrders(Request $request)
    {
        $code = 201;
        $message = 'done.';
        $orders = [];
        try {
            $userID = $this->user->id;
            $conditions = ['user_id' => $this->user->id];
            $whereIn = [];
            if($request->filled('status')) {
                $conditions['status'] = $request->status;
            }
            /* if($request->filled('status')) {
                $whereIn['key'] = 'status';
                switch ($request->status) {
                    case 'sent':
                        $whereIn['values'] = ['preview', 'onway'];
                        break;
                    case 'confirmed':
                        $whereIn['values'] = ['confirmed'];
                        break;
                    case 'completed':
                        $whereIn['values'] = ['completed'];
                        break;
                    case 'rejected':
                        $whereIn['values'] = ['rejected'];
                        break;
                    case 'delivered':
                        $whereIn['values'] = ['delivered'];
                        break;
                    default:
                        $whereIn['values'] = ['preview', 'onway', 'confirmed','completed', 'rejected','delivered'];
                        break;
                }
            } */
            $orders = $this->serviceObj->getAll('Order', $conditions, [], 0, $whereIn);
            $orders->map(function($order) {
                $order->createdAt = Carbon::now()->subDays(1) > $order->created_at ? date('d/m/Y', strtotime($order->created_at)) : $order->created_at->diffForHumans();
            });
        } catch (\Exception $e) {
            $code = getCode($e->getCode()) ;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $orders);
    }

    public function orderDetails(Request $request, $id)
    {
        $code = 201;
        $message = 'done.';
        $order = [];
        try {
            $userID = $this->user->id;
            $conditions = [
                'id' => $id,
                'user_id' => $this->user->id,
            ];
            $order = $this->serviceObj->find('Order', $conditions);
            if(!$order || $order->user_id != $userID) {
                throw new \Exception('not existed!', 404);
            }
            $order->createdAt = Carbon::now()->subDays(1) > $order->created_at ? date('d/m/Y', strtotime($order->created_at)) : $order->created_at->diffForHumans();
            $cart = $order->cart;
            if($cart) {
                $order->products = $cart->products;
            }
            $order->makeHidden(['cart']);
        } catch (\Exception $e) {
            $code = getCode($e->getCode()) ;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $order);
    }
    ##### IN PROGRESS
    public function sendReceipt(Request $request, $id)
    {
        $code = 201;
        $message = 'تم إرسال الفاتورة إلى البريد الإلكترونى.';
        $order = [];

        
        try {
            $order = $this->serviceObj->find('Order',['id' => $id], ['user']);
            if(!$order) {
                throw new \Exception("not found!", 404);
            }
            $order->createdAt = Carbon::now()->subDays(1) > $order->created_at ? date('d/m/Y', strtotime($order->created_at)) : $order->created_at->diffForHumans();
            $cart = $order->cart;
            if($cart) {
                $order->items = $cart->items;
                foreach($order->items as $item) {
                    if($item->product) {
                        $item->product->category = $item->product->category;
                        $item->product->makeHidden(['prices']);
                    }
                }
                $order->makeHidden(['cart']);
            }
            
            $data['order'] = $order;
            $data['email'] = $order->user->email;

            $from = env('MAIL_USERNAME');
            Mail::send('mails.receipt', $data, function ($mail) use ($data, $from) {
                $mail->from($from, 'Fruits App');
                $mail->to($data['email'], 'Fruits App')->subject('Fruits App Syatem');
            });
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $order);
    }
    

    ##### TO DO     
    
	public function deleteOrder(Request $request)
    {
        
    }
    
}
