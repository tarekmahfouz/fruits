<?php

namespace App\Http\Controllers\API;

use App\Models\Product;
use App\Models\PromoCode;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\ConfigResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\CategoryResource;

class PublicController extends Controller
{
    protected $serviceObj;
    protected $user;

    public function __construct(CommonService $serviceObj)
    {
        $this->user = Auth::guard('api')->user();
        $this->serviceObj = $serviceObj;
    }

    # ============ GET METHODS ================
    public function categories(Request $request)
    {
        $code = 200;
        $message = 'done';
        $items = [];
        try {
            $conditions = $whereIn = [];

            $items = $this->serviceObj->getAll('Category', $conditions, [], 0, $whereIn, false);
            $items = CategoryResource::collection($items);
        } catch (\Exception $e) {
            $code = 200;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $items);
    }

    public function productInfo(Request $request, Product $product)
    {
        $code = 200;
        $message = 'done';
        $data = $whereIn = [];
        try {
            #$conditions = ['id' => $id];
            $data['product'] = $product;
            if (!$data['product']) {
                throw new \Exception("not found!", 404);
            }
            $data = new ProductResource($product);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function getBankAccounts(Request $request)
    {
        $code = 200;
        $message = 'done';
        $data = [];
        try {
            $data = $this->serviceObj->getAll('Bank');
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function getAboutUs(Request $request)
    {
        $code = 200;
        $message = 'done';
        try {
            $conditions = ['type' => 'about'];
            //getOne('user' ,$conditions = [], $with = [], $last = false)
            $item = $this->serviceObj->getOne('Content', $conditions, [], true);
            if (!$item) {
                $item = new \stdClass();
                $item->title = "About us";
                $item->content = "Dummy Content";
            }
        } catch (\Exception $e) {
            $code = 500;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $item);
    }
    
    public function availability(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data = $this->serviceObj->getAll('Availability');
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function configs(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $item = $this->serviceObj->find('Config');
            $data = new ConfigResource($item);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function code($promoCode)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $conditions = ['code' => $promoCode, 'status' => 1];
            if (!$data = $this->serviceObj->find('PromoCode', $conditions)) {
                throw new \Exception("Promo Code not valid", 404);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }


    # ============ POST METHODS ================

    public function products(Request $request)
    {
        $code = 200;
        $message = 'done';
        $data = $whereIn = [];
        try {
            $conditions = [];
            if ($request->filled('name')) {
                $conditions[] = ['name', 'LIKE', '%' . $request->name . '%'];
            }
            if ($request->filled('category_id')) {
                $conditions['category_id'] = $request->category_id;
            }
            $products = $this->serviceObj->getAll(
                'Product',
                $conditions,
                $with = [],
                $perPage = 0,
                $whereIn,
            );

            $collection = ProductResource::collection($products);
            $data = paginate($collection, $perPage = 10, $page = request('page'), route('get.products'));
        } catch (\Exception $e) {
            $code = 200;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function sendContactUs(Request $request)
    {
        $code = 201;
        $message = 'done.';
        try {
            $request->validate([
                'name' => 'required',
                'phone' => 'required|numeric',
                'message' => 'required',
            ]);
            $data = $request->only(['name', 'phone', 'message']);
            $this->serviceObj->create('Contact', $data);
            return jsonResponse($code, $message);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
            return jsonResponse($code, $message);
        }
    }

    public function login(Request $request)
    {
        $code = 201;
        $message = 'done.';
        $user = [];
        try {
            $credentials = $request->only(['phone', 'password']);
            if (!$access_token = Auth::guard('api')->attempt($credentials)) {
                throw new \Exception('credentials error!', 400);
            }
            $user = Auth::guard('api')->user();
            $user['access_token'] = $access_token;
        } catch (\Exception $e) {
            $code = 400;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $user);
    }

    public function signUp(Request $request)
    {
        $code = 201;
        $message = 'done.';
        $result = [];
        try {
            $data = $request->except(['password_confirmation',]);

            if ($request->hasFile('image')) {
                $data['image'] = uploadFile($request->file('image'), 'images/users');
            }
            $credentials = $request->only(['phone']);
            $credentials['code'] = $data['code'] = generateCode('User', 4, true);
            $result = $this->serviceObj->create('User', $data);
            if ($result) {
                $result['access_token'] = Auth::guard('api')->login($result);
            } else {
                throw new \Exception('something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = 500;
            $message = $e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine();
        }
        return jsonResponse($code, $message, $result);
    }

    public function forgetPasswordRequest(Request $request)
    {
        $message = 'check your email to get your code.';
        $code = 200;
        try {
            $data['email'] = $request->email;
            $type = $request->type;

            $data['user'] = $this->serviceObj->find('User', ['email' => $request->email]);
            if (!$data['user']) {
                return jsonResponse(404, 'غير موجود!');
            }
            $existed = true;
            $data['code'] = generateCode('user', 4, true);
            $this->serviceObj->update('user', ['email' => $request->email], ['code' => $data['code']]);

            $from = env('MAIL_USERNAME');
            Mail::send('mails.request-resitting', $data, function ($mail) use ($data, $from) {
                $mail->from($from, 'Fruits App');
                $mail->to($data['email'], 'Fruits App')->subject('Fruits App Syatem');
            });
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $code = getCode($e->getCode());
        }
        return jsonResponse($code, $message);
    }

    public function resetPassword(Request $request)
    {
        $message = 'تم إعادة تعيين كلمة المرور.';
        $code = 200;
        try {
            $type = $request->type;
            $request->validate([
                'password' => 'required|min:6|confirmed',
            ]);
            $conditions = ['email' => $request->email, 'code' => $request->code];

            $user = $this->serviceObj->find('User', $conditions);
            if (!$user) {
                $message = 'تأكد من البيانات المدخلة!';
                throw new \Exception($message, 404);
            }
            $data['password'] = bcrypt($request->password);
            $data['code'] = null;
            $this->serviceObj->update('user', $conditions, $data);
            #$user->touch(); // Update updated_at field
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $code = getCode($e->getCode());
        }
        return jsonResponse($code, $message);
    }
}
