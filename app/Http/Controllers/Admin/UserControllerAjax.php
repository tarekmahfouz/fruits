<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Mail;

class UserControllerAjax extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function users(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data['admin'] = $this->user;
            $data['request'] = $request;
            $data['perPage'] = 0;

            $conditions = $multiKeysSearch = [];
            if($request->filled('status') && $request->status != null)
                $conditions['status'] = $request->status;
            if($request->filled('key') && $request->key != null) {
                $multiKeysSearch['key'] = $request->key;
                $multiKeysSearch['fields'] = [
                    ['field' => 'name', 'like'=>true],
                    ['field' => 'phone', 'like'=>true],
                    ['field' => 'email', 'like'=>true],
                ];
            }

            $data['items'] = $this->serviceObj->getAll('User',$conditions, [], $data['perPage'],[],false,$multiKeysSearch);
            $data['page'] = $request->has('page') ? $request->page : 1;
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function getUser(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data = $this->serviceObj->find('User',['id' => $id]);
            if(!$data) {
                throw new \Exception("not found!", 404);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function addUser(UserRequest $request)
    {
        $code = 200;
        $message = "تمت الإضافة بنجاح";
        $data = [];
        try {
            $data = $request->except(['image','password_confirmation','_method','_token']);
            $data['password'] = bcrypt($data['password']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/users');
            }
            $data = $this->serviceObj->create('User', $data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function editUser(UserRequest $request, $id)
    {
        $code = 200;
        $message = 'تم التعديل بنجاح';
        $data = [];
        try {
            $adminID = $request->id;
            //$data = $request->all();
            $data = $request->except(['image','password_confirmation','_method','_token']);
            if($data['password'] == null)
                unset($data['password']);
            else
                $data['password'] = bcrypt($data['password']);
            
            $admin = $this->serviceObj->find('User', ['id' => $adminID]);
            if(!$admin) {
                throw new \Exception("not found!", 404);
            }
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/users', true, $admin->image);
            }
            $data = $this->serviceObj->update('User', ['id' => $adminID],$data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function deleteUser(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        try {
            $response = $this->serviceObj->destroy('User',['id' => $id]);
            if(!$response) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }

    public function sendNotifications(Request $request)
    {
        $code = 200;
        $message = "تمت الإضافة بنجاح";
        $data = [];
        try {
            $data['content'] = $request->notification_content;
            $from = env('MAIL_USERNAME');

            $selected_users = json_decode($request->selected_users);
            
            if(is_array($selected_users)) {
                if(in_array(0, $selected_users)) {
                    $users = $this->serviceObj->getAll('User');
                } else {
                    $users = $this->serviceObj->getAll('User', [],[],0,['key' => 'id', 'values' => $selected_users]);
                }
                foreach($users as $user) {
                    if($user) {
                        $data['email'] = $user->email;
                        Mail::send('mails.notification', $data, function ($mail) use($data, $from) {
                            $mail->from($from, 'Fruits App');
                            $mail->to($data['email'], 'Fruits App')->subject('Fruits App Syatem');
                        });
                    }
                }
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

}
