<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminControllerAjax extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function admins(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data['admin'] = $this->user;
            $data['request'] = $request;
            $data['perPage'] = 0;

            $conditions = $multiKeysSearch = [];
            $conditions[] = ['email','!=','dev@dev.com'];
            if($request->filled('status') && $request->status != null)
            $conditions['status'] = $request->status;
            if($request->filled('key') && $request->key != null) {
                $multiKeysSearch['key'] = $request->key;
                $multiKeysSearch['fields'] = [
                    ['field' => 'name', 'like'=>true],
                    ['field' => 'phone', 'like'=>true],
                    ['field' => 'email', 'like'=>true],
                ];
            }

            $data['items'] = $this->serviceObj->getAll('Admin',$conditions, [], $data['perPage'],[],false,$multiKeysSearch);
            $data['page'] = $request->has('page') ? $request->page : 1;
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function me(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data = Auth::guard('admin')->user();
            if(!$data) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function getAdmin(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data = $this->serviceObj->find('Admin',['id' => $id]);
            if(!$data) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function deleteAdmin(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        try {
            if( $this->user->id == $id) {
                throw new \Exception("can't delete yourself", 400);
            }

            $response = $this->serviceObj->destroy('Admin',['id' => $id]);
            if(!$response) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }
    public function addAdmin(AdminRequest $request)
    {
        $code = 200;
        $message = "تمت الإضافة بنجاح";
        $data = [];
        try {
            $data = $request->except(['image','password_confirmation','_method','_token']);
            $data['password'] = bcrypt($data['password']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/users');
            }
            $data = $this->serviceObj->create('Admin', $data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function editAdmin(AdminRequest $request, $id)
    {
        $code = 200;
        $message = 'تم التعديل بنجاح';
        $data = [];
        try {
            $adminID = $request->id;
            //$data = $request->all();
            $data = $request->except(['image','password_confirmation','_method','_token']);
            if($data['password'] == null)
                unset($data['password']);
            else
                $data['password'] = bcrypt($data['password']);
            
            $admin = $this->serviceObj->find('Admin', ['id' => $adminID]);
            if(!$admin) {
                throw new \Exception("not found!", 404);
            }
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/users', true, $admin->image);
            }
            $data = $this->serviceObj->update('Admin', ['id' => $adminID],$data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

}
