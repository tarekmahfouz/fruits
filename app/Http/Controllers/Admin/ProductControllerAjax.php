<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;

class ProductControllerAjax extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getProducts(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data['admin'] = $this->user;
            $data['request'] = $request;
            $data['perPage'] = $request->perPage ?? 0;

            $conditions = $multiKeysSearch = [];
            if($request->filled('category_id') && $request->category_id != 0)
                $conditions['category_id'] = $request->category_id;
            if($request->filled('key') && $request->key != null) {
                $multiKeysSearch['key'] = $request->key;
                $multiKeysSearch['fields'] = [
                    ['field' => 'name', 'like'=>true],
                ];
            }

            $data['items'] = $this->serviceObj->getAll('Product',$conditions, ['category'], $data['perPage'],[],false,$multiKeysSearch);
            $data['page'] = $request->has('page') ? $request->page : 1;
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function getProduct(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        $item = [];
        try {
            $item = $this->serviceObj->find('Product',['id' => $id], ['category']);
            $item = new ProductResource($item);
            if(!$item) {
                throw new \Exception("not found!", 404);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $item);
    }
    public function createProduct(ProductRequest $request)
    {
        $code = 200;
        $message = 'تمت الإضافة بنجاح.';
        try {
            $data = $request->except(['image','weights']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/products');
            }
            $model = $this->serviceObj->create('Product', $data);
            
            $weights = json_decode($request->weights);
            $weightsData = [];
            if(count($weights)) {
                foreach($weights as $weight) {
                    $item = [];
                    $item['product_id'] = $model->id;
                    $item['price'] = $weight->weight_price;
                    $item['start_from'] = $weight->start_from;
                    $item['weight_unit_id'] = $weight->weight_id;
                    if($item['price'] && $item['weight_unit_id'])
                        $weightsData[] = $item;
                }
            }

            $this->serviceObj->bulkInsert('ProductWeightUnit', $weightsData);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }
    public function editProduct(ProductRequest $request, $id)
    {
        $code = 200;
        $message = 'تم التعديل بنجاح';
        $data = [];
        try {
            $weights = json_decode($request->weights);
            $weightsData = [];
            if(count($weights)) {
                foreach($weights as $weight) {
                    $item = [];
                    $item['product_id'] = $id;
                    $item['price'] = $weight->weight_price;
                    $item['start_from'] = $weight->start_from;
                    $item['weight_unit_id'] = $weight->weight_id;
                    if($item['price'] && $item['weight_unit_id'])
                        $weightsData[] = $item;
                }
            }
            //return jsonResponse($code, $message, $weights);
            
            $data = $request->except(['image','_method','weights']);

            $item = $this->serviceObj->find('Product', ['id' => $id]);
            
            if(!$item) {
                throw new \Exception("not found!", 404);
            }
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/products', true, $item->image);
            }
            $data = $this->serviceObj->update('Product', ['id' => $id],$data);
            $this->serviceObj->destroy('ProductWeightUnit', ['product_id' => $id]);
            $this->serviceObj->bulkInsert('ProductWeightUnit', $weightsData);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function deleteProduct(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        try {
            $response = $this->serviceObj->destroy('Product',['id' => $id]);
            if(!$response) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }
    public function deleteProductPrice(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        try {
            $response = $this->serviceObj->destroy('ProductPrice',['id' => $id]);
            if(!$response) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }

    # ORDERS
    public function getOrdersCount(Request $request)
    {
        $code = 201;
        $message = 'done.';
        $data = [];
        try {
            $conditions = [];
            $whereIn = [
                'key' => 'status',
                'values' => ['preview', 'ready']
            ];
            $data['ordersCount'] = count($this->serviceObj->getAll('Order', $conditions, [], 0, $whereIn));
        } catch (\Exception $e) {
            $code = getCode($e->getCode()) ;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function getOrders(Request $request)
    {
        $code = 201;
        $message = 'done.';
        $data = [];
        try {
            $data['perPage'] = $request->perPage ?? 0;
            $conditions = [];
            $whereIn = [];
            if($request->filled('status') && $request->status != null) {
                $conditions['status'] = $request->status;
            }
            $data['orders'] = $this->serviceObj->getAll('Order', $conditions, ['user'], $data['perPage'], $whereIn);
            $data['orders']->map(function($order) {
                $order->createdAt = Carbon::now()->subDays(1) > $order->created_at ? date('d/m/Y', strtotime($order->created_at)) : $order->created_at->diffForHumans();
                $cart = $order->cart;
                if($cart) {
                    $order->items = $cart->items;
                    foreach($order->items as $item) {
                        if($item->product) {
                            $item->product->makeHidden(['prices']);
                        }
                    }
                    $order->makeHidden(['cart']);
                }
            });
        } catch (\Exception $e) {
            $code = getCode($e->getCode()) ;
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function getOrder(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        $order = [];
        try {
            $order = $this->serviceObj->find('Order',['id' => $id], ['user']);
            if(!$order) {
                throw new \Exception("not found!", 404);
            }
            $order->createdAt = Carbon::now()->subDays(1) > $order->created_at ? date('d/m/Y', strtotime($order->created_at)) : $order->created_at->diffForHumans();
            $cart = $order->cart;
            if($cart) {
                $order->items = $cart->items;
                foreach($order->items as $item) {
                    if($item->product) {
                        $item->product->category = $item->product->category;
                        $item->product->makeHidden(['prices']);
                    }
                }
                $order->makeHidden(['cart']);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $order);
    }
    public function editOrder(ProductRequest $request, $id)
    {
        $code = 200;
        $message = 'تم التعديل بنجاح';
        $data = [];
        try {
            $data = $request->only(['status']);
            $item = $this->serviceObj->find('Order', ['id' => $id]);
            
            if(!$item) {
                throw new \Exception("not found!", 404);
            }
            $data = $this->serviceObj->update('Order', ['id' => $id],$data);

            # Send FCM
            $title = 'الفواكة والخضار';
            $body = 'تم تغيير حالة الطلب';
            $type = 'admin';
            $token = $item->user->firebase;
            FCMPush($title, $body, $type, $token);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

}
