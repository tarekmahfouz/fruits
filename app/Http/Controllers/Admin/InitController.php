<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class InitController extends Controller
{
    public $serviceObj;
    public $data;
    public $user;
    public function __construct()
    {
        app()->setLocale('ar');
        Carbon::setLocale('ar');

        $this->middleware('auth:admin', [
            'except'=>['login','submitLogin']
        ]);
        $this->middleware(function($request, $next) {
            $this->user = Auth::guard('admin')->user();
            $this->serviceObj = new CommonService;
            //$this->data['all_cities'] = $this->serviceObj->getAll('City');
            $this->data['auther'] = $this->user;

            view()->share($this->data);
            return $next($request);
        });

    }
}
