<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function login(Request $request)
    {
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin.home');
        }
        return view('admin.login');
    }
    public function submitLogin(Request $request)
    {
        $credentials = ['email' => $request->email, 'password' => $request->password];
        if (Auth::guard('admin')->attempt($credentials, true)) {
            $admin = Auth::guard('admin')->user();

            return redirect()->route('admin.home');
        }
        return redirect()->back()->withInput($request->all());
    }
}
