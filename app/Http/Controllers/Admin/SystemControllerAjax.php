<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SystemControllerAjax extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function home(Request $request)
    {
        $admin = $this->user;

        $data['admins'] = count($this->serviceObj->getAll('Admin',[['email','!=','dev@dev.com']]));
        $data['users']  = count($this->serviceObj->getAll('User'));

        return view('admin.home')->with($data);
    }


    public function categories(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data['admin'] = $this->user;
            $data['request'] = $request;
            $data['perPage'] = $request->perPage ?? 0;
            //return $conditions;
            $conditions = $multiKeysSearch = [];
            
            if($request->filled('key')) {
                $multiKeysSearch['key'] = $request->key;
                $multiKeysSearch['fields'] = [
                    ['field' => 'name', 'like'=>true],
                ];
            }

            $data['items'] = $this->serviceObj->getAll('Category',$conditions, [], $data['perPage'],[],false,$multiKeysSearch);
            foreach($data['items'] as $item) {
                $item['products_count'] = count($item->products);
                $item->makeHidden(['children']);
            }
            $data['page'] = $request->has('page') ? $request->page : 1;
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function addCategory(Request $request)
    {
        $code = 200;
        $message = "تمت الإضافة بنجاح";
        $data = [];
        try {
            $request->validate([
                'name' => 'required'
            ]);
            $data = $request->except(['image','_token']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/categories');
            }
            $data = $this->serviceObj->create('Category', $data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function editCategory(Request $request, $id)
    {
        $code = 200;
        $message = 'تم التعديل بنجاح';
        $data = [];
        try {
            $itemID = $request->id;
            //$data = $request->all();
            $data = $request->except(['image','_method','_token']);
            $item = $this->serviceObj->find('Category', ['id' => $itemID]);
            if(!$item) {
                throw new \Exception("not found!", 404);
            }
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/categories', true, $item->image);
            }
            $data = $this->serviceObj->update('Category', ['id' => $itemID],$data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function deleteCategory(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        try {
            $response = $this->serviceObj->destroy('Category',['id' => $id]);
            if(!$response) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }

    # AVAILABILITIES    
    public function availabilities(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data['admin'] = $this->user;
            $data['request'] = $request;
            $data['perPage'] = $request->perPage ?? 0;
            
            $conditions = $multiKeysSearch = [];
            
            $data['items'] = $this->serviceObj->getAll('Availability',$conditions, [], $data['perPage'],[],false,$multiKeysSearch);
            
            $data['page'] = $request->has('page') ? $request->page : 1;
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function addAvailability(Request $request)
    {
        $code = 200;
        $message = "تمت الإضافة بنجاح";
        $data = [];
        try {
            $request->validate([
                'time_from' => 'required',
                'time_to' => 'required'
            ]);
            $data = $request->except(['_token']);
            $data = $this->serviceObj->create('Availability', $data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function editAvailability(Request $request, $id)
    {
        $code = 200;
        $message = 'تم التعديل بنجاح';
        $data = [];
        try {
            $itemID = $request->id;
            //$data = $request->all();
            $data = $request->except(['_method','_token']);
            $item = $this->serviceObj->find('Availability', ['id' => $itemID]);
            if(!$item) {
                throw new \Exception("not found!", 404);
            }
            
            $data = $this->serviceObj->update('Availability', ['id' => $itemID],$data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function deleteAvailability(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        try {
            $response = $this->serviceObj->destroy('Availability',['id' => $id]);
            if(!$response) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }

    # WEIGHT UNITS    
    public function weights(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data['admin'] = $this->user;
            $data['request'] = $request;
            $data['perPage'] = $request->perPage ?? 0;
            
            $conditions = $multiKeysSearch = [];
            
            $data['items'] = $this->serviceObj->getAll('WeightUnit',$conditions, [], $data['perPage'],[],false,$multiKeysSearch);
            
            $data['page'] = $request->has('page') ? $request->page : 1;
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function addWeight(Request $request)
    {
        $code = 200;
        $message = "تمت الإضافة بنجاح";
        $data = [];
        try {
            $request->validate([
                'name' => 'required',
            ]);
            $data = $request->except(['_token']);
            $data = $this->serviceObj->create('WeightUnit', $data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function editWeight(Request $request, $id)
    {
        $code = 200;
        $message = 'تم التعديل بنجاح';
        $data = [];
        try {
            $itemID = $request->id;
            //$data = $request->all();
            $data = $request->except(['_method','_token']);
            $item = $this->serviceObj->find('WeightUnit', ['id' => $itemID]);
            if(!$item) {
                throw new \Exception("not found!", 404);
            }
            
            $data = $this->serviceObj->update('WeightUnit', ['id' => $itemID],$data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function deleteWeight(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        try {
            $response = $this->serviceObj->destroy('WeightUnit',['id' => $id]);
            if(!$response) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }


    # PROMO CODES
    public function codes(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $data['admin'] = $this->user;
            $data['request'] = $request;
            $data['perPage'] = $request->perPage ?? 0;
            //return $conditions;
            $conditions = $multiKeysSearch = [];
            if($request->filled('key')) {
                $multiKeysSearch['key'] = $request->key;
                $multiKeysSearch['fields'] = [
                    ['field' => 'name', 'like'=>true],
                ];
            }

            $data['items'] = $this->serviceObj->getAll('PromoCode',$conditions, [], $data['perPage'],[],false,$multiKeysSearch);
            
            $data['page'] = $request->has('page') ? $request->page : 1;
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function addCode(Request $request)
    {
        $code = 200;
        $message = "تمت الإضافة بنجاح";
        $data = [];
        try {
            $request->validate([
                'code' => 'required',
                'value' => 'required|numeric'
            ]);
            $data = $request->except(['image','_token']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images');
            }
            $data = $this->serviceObj->create('PromoCode', $data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function editCode(Request $request, $id)
    {
        $code = 200;
        $message = 'تم التعديل بنجاح';
        $data = [];
        try {
            $itemID = $request->id;
            $data = $request->except(['image','_method','_token']);
            $item = $this->serviceObj->find('PromoCode', ['id' => $itemID]);
            if(!$item) {
                throw new \Exception("not found!", 404);
            }
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images', true, $item->image);
            }
            $data = $this->serviceObj->update('PromoCode', ['id' => $itemID],$data);
            
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function deleteCode(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        try {
            $response = $this->serviceObj->destroy('PromoCode',['id' => $id]);
            if(!$response) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }

    
    # CONFIG
    public function getConfig(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $conditions = [];
            $data['items'] = $this->serviceObj->getOne('Config',$conditions);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    
    public function editConfig(Request $request)
    {
        $code = 200;
        $message = 'تم التعديل بنجاح';
        $data = [];
        try {
            $data = $request->except(['_method']);
            $item = $this->serviceObj->getOne('Config');
            //return $request->all();
            if(!$item) {
                $data = $this->serviceObj->create('Config', $data);
            }else {
                $data = $this->serviceObj->update('Config', ['id' => $item->id],$data);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    
    public function getInbox(Request $request)
    {
        $code = 200;
        $message = "done.";
        $data = [];
        try {
            $perPage = $request->perPage ?? 0;
            $conditions = [];
            $data['items'] = $this->serviceObj->getAll('Contact',$conditions, [], $perPage);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }
    public function deleteMessage(Request $request, $id)
    {
        $code = 200;
        $message = "done.";
        try {
            $response = $this->serviceObj->destroy('Contact',['id' => $id]);
            if(!$response) {
                throw new \Exception("something went wrong!", 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }
}
