<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'phone' => 'required|unique:users,phone',
                        'email' => 'required|email|unique:users,email',
                        'name' => 'required|min:2',
                        'password' => 'required|min:6|confirmed',
                        'image' => 'image',
                    ];
                }
            case 'PUT': {
                    if (request()->wantsJson() && !request()->ajax()) {
                        return [
                            'name' => 'nullable|min:2',
                            'phone' => 'required|unique:users,phone,' . $this->user()->id,
                            'email'    => 'nullable|email|unique:users,email,' . $this->user()->id,
                            'password' => 'nullable|min:6|confirmed',
                            'image' => 'nullable|image',
                        ];
                    } else {
                        return [
                            'name' => 'nullable|min:2',
                            'phone' => 'nullable|unique:users,phone,' . $this->segment(3),
                            'email'    => 'nullable|email|unique:users,email,' . $this->segment(3),
                            'password' => 'nullable|min:6|confirmed',
                            'image' => 'nullable|image',
                        ];
                    }
                }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'whatsapp.*' => 'ال Whatsapp غير صالح',
            'lat.*' => 'الموقع الجغرافى غير دقيق',
            'lon.*' => 'الموقع الجغرافى غير دقيق',
            'city_id.*' => 'المدينة غير صحيحة',
            'name.*' => 'الاسم غير صالح',
            'address.*' => 'العنوان غير صالح',
            'username.*' => 'اسم المستخدم غير صالح',
            'phone.required' => 'رقم الجوال مطلوب',
            'phone.digits_between' => 'رقم الجوال غير صالح',
            'phone.unique' => 'رقم الجوال موجود بالفعل',
            'email.required' => 'البريد الإلكترونى مطلوب',
            'email.email' => 'البريد الإلكترونى غير صالح',
            'email.unique' => 'البريد الإلكترونى موجود بالفعل',
            'image.*' => 'الصورة غير صالحة',
            'password.required' => 'الرقم السرى مطلوب',
            'password.min' => 'أقل قيمة للرقم السرى هى 6 حروف',
            'password.confirmed' => 'يجب تأكيد الرقم السرى',
            'category_id.*' => 'الفئة غير صالحة',
            'price.*' => 'السعر غير صالح',
            'category_id.*' => 'الفئة غير صالحة',
            'area_id.*' => 'المنطقة غير صحيحة',
            'user_name.*' => 'اسم المستخدم غير صالح',
            'user_phone.*' => 'رقم الجوال غير صالح',
            'tax_value.*' => 'قيمة الضريبة غير صالحة',
            'description.*' => 'الوصف غير صالح',
        ];
    }
}
