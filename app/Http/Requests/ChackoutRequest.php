<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class ChackoutRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'cart_id' => 'required|exists:carts,id',
                        //'payment_way' => 'required|in:cash,bank',
                        'availability_id' => 'required|exists:availabilities,id',
                        'promo_code' => 'nullable|exists:promo_codes,code',
                        'day' => 'required|date|date_format:Y-m-d|after:yesterday',
                        'lat' => 'required|numeric',
                        'lon' => 'required|numeric',
                        'address' => 'required',
                    ];
                }
            case 'PUT':
                {
                    return [
                        'cart_id' => 'required|exists:carts,id',
                        'payment_way' => 'required|in:cash,bank',
                        //'availability_id' => 'required|exists:availabilities,id',
                        'promo_code' => 'nullable|exists:promo_codes,code',
                        'day' => 'required|date|date_format:Y-m-d',
                        'lat' => 'required|numeric',
                        'lon' => 'required|numeric',
                        'address' => 'required',
                    ];
                }
            default:
                break;
        }
    }


    public function messages()
    {
        return [
            'whatsapp.*' => 'ال Whatsapp غير صالح',
            'lat.*' => 'الموقع الجغرافى غير دقيق',
            'lon.*' => 'الموقع الجغرافى غير دقيق',
            'city_id.*' => 'المدينة غير صحيحة',
            'name.*' => 'الاسم غير صالح',
            'address.*' => 'العنوان غير صالح',
            'username.*' => 'اسم المستخدم غير صالح',
            'phone.required' => 'رقم الجوال مطلوب',
            'phone.digits_between' => 'رقم الجوال غير صالح',
            'phone.unique' => 'رقم الجوال موجود بالفعل',
            'email.required' => 'البريد الإلكترونى مطلوب',
            'email.email' => 'البريد الإلكترونى غير صالح',
            'email.unique' => 'البريد الإلكترونى موجود بالفعل',
            'image.*' => 'الصورة غير صالحة',
            'password.required' => 'الرقم السرى مطلوب',
            'password.min' => 'أقل قيمة للرقم السرى هى 6 حروف',
            'password.confirmed' => 'يجب تأكيد الرقم السرى',
            'category_id.*' => 'الفئة غير صالحة',
            'price.*' => 'السعر غير صالح',
            'category_id.*' => 'الفئة غير صالحة',
            'area_id.*' => 'المنطقة غير صحيحة',
            'user_name.*' => 'اسم المستخدم غير صالح',
            'user_phone.*' => 'رقم الجوال غير صالح',
            'tax_value.*' => 'قيمة الضريبة غير صالحة',
            'description.*' => 'الوصف غير صالح',
            'message.*' => 'الرسالةغير صالحة',
            'cart_id.*' => 'يجب التأكد من عربة الشراء',
            'payment_way.*' => 'طريقة الدفع غير صالحة',
            'promo_code.*' => 'البرومو كدو غير صالح',
            'day.*' => 'التاريخ غير صالح',
            'availability_id.*' => 'تاريخ الصلاحية غير صالح',
        ];
    }
}
