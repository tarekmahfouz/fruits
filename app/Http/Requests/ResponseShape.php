<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
# use Illuminate\Http\JsonResponse;

class ResponseShape extends FormRequest
{

    protected function failedValidation(Validator $validator)
    {
        if(request()->wantsJson()){
          $response = jsonResponse(
            400,
            $validator->errors()->first()
          );
          throw new \Illuminate\Validation\ValidationException($validator, $response);
        }
        else {
          throw (new ValidationException($validator))->errorBag($this->errorBag);
        }
    }
}
