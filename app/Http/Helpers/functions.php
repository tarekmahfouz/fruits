<?php

use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\URL;
use App\Http\Services\CommonService;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

/* function getService() {
    $service = new CommonService();
    return $service;
} */



function jsonResponse($code = 200, $message = '', $data = []) {
	return response()->json([
		'code' => $code,
		'message' => $message,
		'data' => $data ?? [],
	],$code);
}

function uploadFile($file, $path, $edit = false, $oldFile = null) {
    $destination = env('SYSTEM_PATH')().'/'.$path;
    $oldDestination = env('SYSTEM_PATH')().'/'.$path.'/'.$oldFile;
    if($edit && is_file($oldDestination)) {
        $name = explode('.', $oldFile)[0];
        if($name != 'default')
            unlink($oldDestination);
    }
    $ext = $file->getClientOriginalExtension();
    $name = time().Str::random(5);
    $fileName = $name.'.'.$ext;
    $file->move($destination, $fileName);
    return $fileName;
}

function generateCode($model, $count, $numbersOnly = false) {
	$serviceObj = new CommonService;
	$code = $numbersOnly ? rand(pow(10, (int)$count-1), pow(10, (int)$count)-1) : strToUpper(Str::random(((int)$count)));
	$exist = $serviceObj->find($model,['code' => $code]);
	if($exist)
        generateCode($model, $count, $numbersOnly = false);
	return $code;
}


function generateIdentifier($model, $count = 10, $numbersOnly = false) {
	$serviceObj = new CommonService;
	$identifier = $numbersOnly ? rand(((int)$count - 1)*10, pow(10, (int)$count)-1) : strToUpper(Str::random(((int)$count)));
	$exist = $serviceObj->find($model,['identifier' => $identifier]);
	if($exist)
        generateIdentifier($model, $count = 10, $numbersOnly = false);
	return $identifier;
}


function getCode($code) {
    $list = [501,500,415,412,406,405,404,403,401,400,307,304,303,302,301,204,202,201,200,];
    return  in_array($code, $list) ? $code : 400;
}


/* function FCMPush($title, $body, $type, $token = null, $topic = null, $extra = [])
{
    $url = 'https://fcm.googleapis.com/fcm/send';
    $data = array(
        "title" => $title,
        "body"  => $body,
        "type"  => $type,
        //"data"  => $extra,
        'content_available'=> true,
        'vibrate' => 1,
        'sound' => true,
        'priority'=> 'high',
    );
    foreach ($extra as $key => $value) {
      $data[$key] = $value;
    }

    if($topic != null) {
        $fields = array(
            'to' =>'/topics/'.$topic,
            'notification' => $data,
            'priority'=>'high'
        );
    } else {
        $fields = array(
            'to' => $token, 
            'notification' => $data,
            'priority'=>'high'
        );
    }
    $fcmApiKey = env('FIREBASE_TOKEN');
    $headers = array(
        'Authorization: key=' . $fcmApiKey,
        'Content-Type:application/json'
    );
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if ($result === false) {
        die('cUrl faild: '.curl_error($ch));
    }
    curl_close($ch);
    return $result;
}
 */

function FCMPush($title, $body, $type, $token = null, $topic = null, $extra = [])
{
    $fcmApiKey = env('FIREBASE_TOKEN');
    $url  = 'https://fcm.googleapis.com/fcm/send';
    $data = array(
        "title"             => $title,
        "body"              => $body,
        "type"              => $type,
        //"data"  => $extra,
        'content_available' => true,
        'vibrate'           => 1,
        'sound'             => true,
        'priority'          => 'high',
    );
    foreach ($extra as $key => $value) {
        $data[$key] = $value;
    }

    if ($topic != null) {
        $fields = array(
            'to'           => '/topics/' . $topic,
            'notification' => $data,
            'priority'     => 'high'
        );
    } else {
        $notification = array(
            'title'             => $title,
            'body'              => $body,
            'sound'             => 'default',
            'content_available' => true,
            'vibrate'           => 1,
            'sound'             => true,
            'badge'             => 1
        );
        $fields       = array(
            // 'registration_ids' => $token, # Like topic, it Sends To Multiple devices (Accept Array of device tokens)
            'to' => $token, # Send To only one device
            'notification'     => $notification,
            'data'             => ['click_action' => 'FLUTTER_NOTIFICATION_CLICK '],
        );
    }

    $headers = array(
        //'Authorization: key=' . env('FIREBASE_API_TOKEN'),
        'Authorization: key='.$fcmApiKey,
        'Content-Type:application/json'
    );
    //return $fields;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if ($result === false) {
        die('cUrl faild: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}


function lang($keyword) {
    return trans('lang.'.$keyword);
}



function paginate($items, $perPage = 15, $page = null, $baseUrl = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

	$items = $items instanceof Collection ? $items : Collection::make($items);

    $items = new LengthAwarePaginator(array_values($items->forPage($page, $perPage)->toArray()), $items->count(), $perPage, $page, $options);

    if ($baseUrl) {
        $items->setPath($baseUrl);
    }

    return $items;
}