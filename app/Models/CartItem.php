<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
    
    
    public function cart()
    {
        return $this->belongsTo(Cart::class, 'cart_id')->withDefault();
    }
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withDefault();
    }
}
