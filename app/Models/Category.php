<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];
    protected $appends = ['image_path',];

    public function getImagePathAttribute()
    {
        return $this->image ? env('APP_PATH') . '/images/categories/' . $this->image : null;
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }
}
