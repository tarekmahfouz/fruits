<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Hash;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    //use SoftDeletes;

    protected $guarded = [];
    protected $appends = ['image_path',];
    protected $hidden = ['password', 'remember_token', 'code', 'email_verified_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];


    public function getImagePathAttribute()
    {
        return $this->image ? env('APP_PATH') . '/images/users/' . $this->image : null;
    }

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }
    
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }
    public function carts()
    {
        return $this->hasMany(Cart::class, 'user_id');
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function($model){
            $model->orders()->delete();
            foreach($model->carts as $cart){
                $cart->delete();
            }
        });
    }

    /************** JWT ****************/
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
