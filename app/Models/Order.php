<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
    

    
    public function cart()
    {
        return $this->belongsTo(Cart::class, 'cart_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }
    public function cartItems()
    {
        return $this->hasManyThrow(CartItem::class, Cart::class)->with('quantity');
    }
}
