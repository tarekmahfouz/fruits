<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];
    

    public function items()
    {
        return $this->hasMany(CartItem::class, 'cart_id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }
    

    public static function boot()
    {
        parent::boot();
        static::deleting(function($model){
            $model->items()->delete();
        });
    }
}
