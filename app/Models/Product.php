<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];
    protected $appends = ['image_path',];

    public function getImagePathAttribute()
    {
        return $this->image ? env('APP_PATH') . '/images/products/' . $this->image : null;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id')->withDefault();
    }
    public function weightUnits()
    {
        return $this->hasMany(ProductWeightUnit::class, 'product_id');
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function($model){
            $model->weightUnits()->delete();
        });
    }
}
