<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductWeightUnit extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];


    public function unit()
    {
        return $this->belongsTo(WeightUnit::class, 'weight_unit_id')->withDefault();
    }
}
