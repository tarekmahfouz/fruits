<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use URL;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guarded = [];
    protected $hidden = ['created_at','updated_at',];
    protected $appends = ['image_path',];
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
    
    # ************** Attributes ****************
    public function getImagePathAttribute()
    {
        return $this->image ? env('APP_URL').'/images/users/'.$this->image : null;
    }
    # ************** JWT ****************
    /* public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    } */
    
}
