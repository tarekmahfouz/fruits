<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at'];

    protected $casts = [
        'status' => 'boolean',
    ];
}
