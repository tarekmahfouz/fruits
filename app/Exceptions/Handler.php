<?php

namespace App\Exceptions;

use Exception;
use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Throwable $exception)
    {
        if($exception instanceof ModelNotFoundException) {
            if($request->wantsJson()){
                return response()->json([
                    'code' => 404,
                    'message' => 'غير موجود.',
                    'data'=> []
                ],401);
            } else {
                return 'not found!';
            }
        }
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request,AuthenticationException $ex)
    {
        if($request->wantsJson()){
            return response()->json([
                'code' => 401,
                'message' => 'Unauthorized.',
                'data' => []
            ],401);
        }
        return redirect()->guest('/admin-panel/login');
    }

}
