<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
    @include('admin.components.head')

        <body class="vertical-layout vertical-menu-modern 1-column   menu-expanded blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
            <div class="app-content content">
              <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">

                @yield('content')
        </div>
          </div>
            </div>



    <!-- ######### Java Script Tarek Mahfouz-->
    <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/icheck.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/app.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/form-login-register.js') }}"></script>
    @stack('scripts')

  </body>
</html>
