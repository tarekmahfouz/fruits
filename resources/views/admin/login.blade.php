@extends('admin.layout.app')


@section('title',trans('lang.login'))



@section('content')
<section class="flexbox-container" >
		<div class="col-12 d-flex align-items-center justify-content-center">
				<div class="col-md-4 col-10 box-shadow-2 p-0">
					 <div class="card border-grey  m-0 border-top-success box-shadow-0 ">
								<div class="card-header border-0 ">
                    <div class="card-title text-center">
                        <div class="p-1"><a href="http://www.tamkeen-apps.com" target="_blank"><img src="{{ asset('app-assets/Tamkeen_Logo.png') }}" style="width:120px; height:70px; " alt=""></a></div>
                    </div>
                    <h5 class="card-subtitle text-muted text-center  pt-2 ">لوحه تحكم الاداره العامه للبرنامج</h5>
                </div>
								<div class="card-content">
										<div class="card-body">
												{{ Form::open(['route' => 'admin.submit.login', 'method' => 'post']) }}
														<fieldset class="form-group position-relative has-icon-left mb-1">
																<input name="email" type="text" class="form-control form-control"  placeholder="اسم المستحدم" required>
																<div class="form-control-position">
																		<i class="ft-user"></i>
																</div>
														</fieldset>
														<fieldset class="form-group position-relative has-icon-left">
																<input name="password" type="password" class="form-control form-control"  placeholder="كلمه المرور" required>
																<div class="form-control-position">
																		<i class="la la-key"></i>
																</div>
														</fieldset>
														<div class="form-group row ">
																<div class="col-md-4 col-12 text-center text-md-left">
																		<fieldset>
																				<input type="checkbox"  class="chk-remember">
																				<label class=" ml-1"> تذكرني</label>
																		</fieldset>
																</div>
																
																							<div class="col-md-8 col-12 text-center text-md-left">
                                    <button type="submit" class="btn btn-success  btn-block ">دخول الاداره العامه</button>
                                </div>
														</div>
												

												{{ Form::close() }}
										</div>
								</div>

						</div>
				</div>
		</div>

</section>
@endsection
