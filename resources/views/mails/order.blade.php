<style>
    table{
        margin: 10px;
        padding: 15px;
        width: 100%;
    }
    th,td{
        width: 100px;
        padding: 10px;
    }
</style>

<header>
    تم استلام طلب جديد برقم
</header>

<br><br>

<table 
        border="1" 
        style="margin: 10px; padding: 15px; width: 100%;"
    >
    <thead>
        <tr>
            <th>رقم الطلب</th>
            <th>تاريخ الطلب</th>
            <th>إجمالى المبلغ</th>
            <th>طريقة الدفع</th>
            <th>قيمة الشحن</th>
            <th>كود الخصم</th>
            <th>الحالة</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $order->id }}</td>
            <td>{{ $order->created_at->format('Y-m-d') }}</td>
            <td>{{ $order->total_cost }}</td>
            <td>{{$order->payment_way}}</td>
            <td>{{ $order->shipping_value }}</td>
            <td>{{ $order->promo_code }}</td>
            <td>{{ $order->status }}</td>
        </tr>
    </tbody>
</table>

<br><br>

<table 
        border="1" 
        style="margin: 10px; padding: 15px; width: 100%;"
    >
    <thead>
        <tr>
            <th>ملاحظات العميل</th>
            <th>وقت التوصيل</th>
            <th>موعد التوصيل</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $order->notes }}</td>
            <td>{{ $order->time_from ." - ".$order->time_to }}</td>
            <td>{{ $order->day }}</td>
        </tr>
    </tbody>
</table>

<br><br>

<table 
        border="1" 
        style="margin: 10px; padding: 15px; width: 100%;"
    >
    <thead>
        <tr>
            <th>اسم المستخدم</th>
            <th>رقم الجوال</th>
            <th>البريد الإلكترونى</th>
            <th>العنوان</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $order->user->name }}</td>
            <td>{{ $order->user->phone }}</td>
            <td>{{ $order->user->email }}</td>
            <td>{{ $order->address }}</td>
        </tr>
    </tbody>
</table>

<br><br>

<table 
        border="1" 
        style="margin: 10px; padding: 15px; width: 100%;"
    >
    <thead>
        <tr>
            <th>اسم المنتج</th>
            <th>الوحدة / الوزن</th>
            <th>يبدأ من</th>
            <th>السعر</th>
            <th>الكمية</th>
            <th>إجمالى</th>
        </tr>
    </thead>
    <tbody>
        @foreach($order->cart->items as $item)
        <tr>
            <td>{{ $item->product->name }}</td>
            <td>{{ $item->product_weight_unit }}</td>
            <td>{{ $item->start_from . " " . $item->product_weight_unit }}</td>
            <td>{{ $item->price }}</td>
            <td>{{ ($item->quantity * $item->start_from) ." ". $item->product_weight_unit }}</td>
            <td>{{ $item->total_price }}</td>
        </tr>
        @endforeach
    </tbody>
</table>