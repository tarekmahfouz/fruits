import React, { Component, createContext } from 'react';

export const SystemContext = createContext();

class SystemContextProvider extends Component {
    state = {
        adminID:0,
        adminName: '',
        adminImage:'',
    }
    render() {
        return (
            <SystemContext.Provider value={{...this.state}}>
                { this.props.children }
            </SystemContext.Provider>
        );
    }
}
 
export default SystemContextProvider;