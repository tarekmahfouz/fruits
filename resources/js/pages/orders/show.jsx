import React, { Component, useContext } from "react";
import {
  AxiosCall,
  ImageBasePath,
  SendPostRequest,
} from "../../helpers/Shared";

class Order extends Component {
  state = {
    id: this.props.match.params.id,
    order: {},
    isLoading: true,
    items: [],
    address: "",
    payment: [],
    user: [],
    totalCost: 0,
    promo_code: "",
    promo_code_value: 0,
    shipping_value: 0,
    createdAt: "",
    status: "",
    statuses: [
      { name: "preview", translation: "قيد المعاينة" },
      { name: "onway", translation: "جارى التوصيل" },
      { name: "confirmed", translation: "تم التأكيد" },
      { name: "rejected", translation: "مرفوض" },
      { name: "delivered", translation: "تم التوصيل" },
    ],
  };

  getData = () => {
    let endPoint = "get-order/" + this.state.id;
    let response = AxiosCall(endPoint, "GET");
    response
      .then((result) => result.data)
      .then((data) => data.data)
      .then((data) => {
        //console.log("network response ", data);
        this.setState({
          isLoading: false,
          order: data,
          items: data.items,
          user: data.user,
          address: data.address,
          totalCost: data.total_cost,
          promo_code: data.promo_code,
          promo_code_value: data.promo_code_value,
          shipping_value: data.shipping_value,
          status: data.status,
          createdAt: data.createdAt,
        });
      });
  };

  componentWillMount() {
    this.getData();
  }

  viewModal = (viewID) => {
    window.$("#" + viewID).modal("show");
  };

  deleteModal = async (id, modalID) => {
    window.$("#" + modalID).modal("hide");
    let endPoint = "delete-category/" + id;
    await AxiosCall(endPoint, "DELETE");
    this.setState({
      items: this.state.items.filter((item) => item.id !== id),
    });
  };

  handleEditChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  handleSubmitEdit = (e) => {
    e.preventDefault();
    let endPoint = "edit-order/" + this.state.id;
    let data = [
      { key: "_method", value: "PUT" },
      { key: "status", value: this.state.status },
    ];
    let res = SendPostRequest(endPoint, data);
    //let res = SendPostRequest(endPoint, data, headers);
    res.then(
      (response) => {
        window.$("#changeStatusModal").modal("hide");
        alert(response.data.message);
      },
      (error) => {
        alert(error.response.message);
        console.log(error.response);
      }
    );
  };

  translateStatus = (key) => {
    let value = "";
    switch (key) {
      case "preview":
        value = "قيد المعاينة";
        break;
      case "onway":
        value = "جارى التوصيل";
        break;
      case "confirmed":
        value = "تم التأكيد";
        break;
      case "rejected":
        value = "مرفوض";
        break;
      case "delivered":
        value = "تم التوصيل";
        break;
      default:
        value = "قيد المعاينة";
        break;
    }
    return value;
  };
  translatePayment = (key) => {
    let value = "";
    switch (key) {
      case "cash":
        value = "نقدا";
        break;
      case "visa":
        value = "فيزا";
        break;
      case "third-party":
        value = "تحويل بنكى";
        break;
      default:
        value = "نقدا";
        break;
    }
    return value;
  };

  render() {
    let {
      id,
      order,
      isLoading,
      items,
      shipping_value,
      address,
      payment,
      user,
      totalCost,
      status,
      promo_code,
      promo_code_value,
      createdAt,
    } = this.state;

    let statuses = ["preview", "onway", "confirmed", "delivered", "rejected"];
    let editID = "changeStatusModal";

    console.log("Render State => ", this.state);
    let renderItems = items.map((item) => {
      return (
        <div className="card pull-up">
          <div className="card-header"></div>
          <div className="card-content">
            <div className="card-body py-2">
              <div className="d-flex justify-content-between lh-condensed">
                <div className="order-details text-center">
                  <div className="d-flex align-items-center">
                    <img
                      className="img-fluid"
                      style={{
                        maxWidth: "150px",
                        maxHeight: "150px",
                        borderRadius: "10px",
                      }}
                      src={ImageBasePath + "/products/" + item.product.image}
                    />
                  </div>
                </div>
                <div className="order-details">
                  <h6 className="my-0">{item.product.name}</h6>
                  <small className="text-muted">
                    {item.product.category.name}
                  </small>
                </div>
                <div className="order-details">
                  <h6 className="my-0">{"الوحدة / الوزن"}</h6>
                  <small className="text-muted">
                    {item.product_weight_unit}
                  </small>
                </div>
                <div className="order-details">
                  <div className="order-info">{item.price} ر.س</div>
                </div>
              </div>
            </div>
          </div>
          <div className="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
            <span className="float-left">
              <span className="text-muted">الكمية : </span>
              <strong>
                {item.quantity * item.start_from +
                  " " +
                  item.product_weight_unit}
              </strong>
            </span>
            <span className="float-right">
              <span className="text-muted">إجمالى : </span>
              <strong>{item.total_price} ر.س</strong>
            </span>
          </div>
        </div>
      );
    });
    const renderEditModal = (
      <div
        className="modal fade text-left"
        id={editID}
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myModalLabel1"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <h4 className="text-center mb-2">تغيير حالة الطلب</h4>
              <form onSubmit={this.handleSubmitEdit}>
                <table className="table">
                  <tbody>
                    <tr>
                      <td>
                        <label htmlFor="">الحالة</label>
                        <select
                          value={status}
                          onChange={this.handleEditChange}
                          className="form-control"
                          name="status"
                          id=""
                        >
                          {this.state.statuses.map((st) => {
                            return (
                              <option key={st.name} value={st.name}>
                                {st.translation}
                              </option>
                            );
                          })}
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <input
                          type="submit"
                          className="btn btn-success"
                          value="حفظ"
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </form>
            </div>
            <div className="modal-footer bg-flight">
              <button
                type="button"
                className="btn grey btn-outline-light"
                data-dismiss="modal"
              >
                <i className="la la-times-circle pr-1"></i>اغلاق
              </button>
            </div>
          </div>
        </div>
      </div>
    );
    return (
      <div className="app-content content">
        <div className="content-wrapper">
          <div className="content-body">
            <div
              className="tab-pane active"
              id="comp-order-tab"
              aria-expanded="true"
              role="tablist"
              aria-labelledby="complete-order"
            >
              <div className="card">
                <div className="card-content">
                  <div className="card-header">
                    <div className="float-left">
                      <h4 className="mb-0">
                        <strong>بيانات الطلب</strong>
                      </h4>
                    </div>
                    <div className="float-right">
                      <button
                        type="button"
                        className="btn btn-icon btn-warning  box-shadow-1 mr-1 mb-1"
                        title=""
                        data-original-title="تعديل"
                        onClick={() => this.viewModal(editID)}
                      >
                        <i className="la la-edit"></i>
                      </button>
                      {renderEditModal}
                    </div>
                  </div>
                  <div className="card-body">
                    <div className="d-flex justify-content-around lh-condensed">
                      <div className="order-details text-center">
                        <div className="order-title">رقم الطلب</div>
                        <div className="order-info">#{id}</div>
                      </div>
                      <div className="order-details text-center">
                        <div className="order-title">تاريخ الطلب</div>
                        <div className="order-info">{createdAt}</div>
                      </div>
                      <div className="order-details text-center">
                        <div className="order-title">إجمالى المبلغ</div>
                        <div className="order-info">ر.س {totalCost}</div>
                      </div>
                      <div className="order-details text-center">
                        <div className="order-title">طريقة الدفع</div>
                        <div className="order-info">
                          {this.translatePayment("cash")}
                        </div>
                      </div>
                      <div className="order-details text-center">
                        <div className="order-title">قيمة الشحن</div>
                        <div className="order-info">{shipping_value}</div>
                      </div>
                      <div className="order-details text-center">
                        <div className="order-title">كود الخصم</div>
                        <div className="order-info">{promo_code}</div>
                        <div className="order-info">{promo_code_value} ر.س</div>
                      </div>
                      <div className="order-details text-center">
                        <div className="order-title">الحالة</div>
                        <div className="order-info">
                          {this.translateStatus(status)}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* <div className="card">
                <div className="card-header">
                  <h4 className="mb-0">
                    <strong>تفاصيل الشحن</strong>
                  </h4>
                </div>
                <div className="card-body">
                  <div className="d-flex justify-content-around lh-condensed">
                    <div className="order-details text-center">
                      <div className="order-title">العنوان</div>
                      <div className="order-info">{address}</div>
                    </div>
                  </div>
                </div>
              </div> */}

              <div className="card">
                <div className="card-header">
                  <h4 className="mb-0">
                    <strong>بيانات الطلب</strong>
                  </h4>
                </div>
                <div className="card-body">
                  <div className="d-flex justify-content-around lh-condensed">
                    <div className="order-details text-center">
                      <div className="order-title">ملاحظات العميل</div>
                      <div className="order-info">{order.notes}</div>
                    </div>
                    <div className="order-details text-center">
                      <div className="order-title">وقت التوصيل</div>
                      <div className="order-info">
                        من {order.time_from} إلى {order.time_to}
                      </div>
                    </div>
                    <div className="order-details text-center">
                      <div className="order-title">موعد التوصيل</div>
                      <div className="order-info">{order.day}</div>
                    </div>
                    <div className="order-details text-center">
                      <div className="order-title">العنوان على الخريطة</div>
                      <div className="order-info">
                        <a
                          target="_blank"
                          href={`https://www.google.com/maps/@${order.lat},${order.lon},19z`}
                        >
                          جوجل ماب
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="card">
                <div className="card-header">
                  <h4 className="mb-0">
                    <strong>بيانات المستخدم</strong>
                  </h4>
                </div>
                <div className="card-body">
                  <div className="d-flex justify-content-around lh-condensed">
                    <div className="order-details text-center">
                      <div className="order-title">اسم المستخدم</div>
                      <div className="order-info">
                        <img
                          style={{
                            height: "30px",
                            width: "30px",
                            borderRadius: "15px",
                            margin: "10px",
                          }}
                          src={ImageBasePath + "/users/" + user.image}
                          alt=""
                        />
                        {user.name}
                      </div>
                    </div>
                    <div className="order-details text-center">
                      <div className="order-title">رقم الجوال</div>
                      <div className="order-info">{user.phone}</div>
                    </div>
                    <div className="order-details text-center">
                      <div className="order-title">البريد الإلكترونى</div>
                      <div className="order-info">{user.email}</div>
                    </div>
                    <div className="order-details text-center">
                      <div className="order-title">العنوان</div>
                      <div className="order-info">{address}</div>
                    </div>
                  </div>
                </div>
              </div>

              {renderItems}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Order;
