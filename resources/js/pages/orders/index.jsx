import React, { Component, useContext } from "react";
import { Link } from "react-router-dom";
import {
  AxiosCall,
  ImageBasePath,
  SendPostRequest,
} from "../../helpers/Shared";
import Breadcrumb from "./../../components/Breadcrumb";
import { v1 as uuidv1 } from "uuid";

class Orders extends Component {
  state = {
    itemStatus: "",
    isLoading: true,
    items: null,
    perPage: 10,
    page: 0,
    last_page: "",
    first_page_url: "",
    last_page_url: "",
    next_page_url: "",
    prev_page_url: "",
  };

  getData = (itemStatus = null) => {
    let endPoint = `get-orders`;
    endPoint += itemStatus === null ? "?status=" : `?status=${itemStatus}`;

    let response = AxiosCall(endPoint, "GET");
    response
      .then((result) => result.data)
      .then((data) => data.data)
      .then((data) => {
        this.setState({
          isLoading: false,
          perPage: data.perPage,
          items: data.perPage > 0 ? data.orders.data : data.orders,
          last_page: data.perPage > 0 ? data.last_page : "",
          next_page_url: data.perPage > 0 ? data.next_page_url : "",
          prev_page_url: data.perPage > 0 ? data.prev_page_url : "",
          first_page_url: data.perPage > 0 ? data.first_page_url : "",
          last_page_url: data.perPage > 0 ? data.last_page_url : "",
          page: data.page,
        });
      });
  };

  componentWillMount() {
    this.getData();
  }

  viewModal = (viewID) => {
    window.$("#" + viewID).modal("show");
  };

  deleteModal = async (id, modalID) => {
    window.$("#" + modalID).modal("hide");
    let endPoint = "delete-category/" + id;
    await AxiosCall(endPoint, "DELETE");
    this.setState({
      items: this.state.items.filter((item) => item.id !== id),
    });
  };

  handleEditChange = (toUpdateItem) => (e) => {
    if (e.target.name == "status") {
      toUpdateItem.status = e.target.value;
    }
    if (this.state.items.length > 0) {
      this.setState((state) => {
        const items = state.items.map((item) => {
          if (item.id === toUpdateItem.id) {
            return toUpdateItem;
          } else {
            return item;
          }
        });
        return {
          items,
        };
      });
    }
  };
  handleSubmitEdit = (item) => (e) => {
    e.preventDefault();
    let endPoint = "edit-order/" + item.id;
    let data = [
      { key: "_method", value: "PUT" },
      { key: "status", value: item.status },
    ];
    let res = SendPostRequest(endPoint, data);
    //let res = SendPostRequest(endPoint, data, headers);
    res.then(
      (response) => {
        alert(response.data.message);
        console.log(response);
        window.$("#editModal_" + item.id).modal("hide");
      },
      (error) => {
        alert(error.response.message);
        console.log(error.response);
      }
    );
  };

  translateStatus = (key) => {
    let value = "";
    switch (key) {
      case "preview":
        value = "قيد المعاينة";
        break;
      case "onway":
        value = "جارى التوصيل";
        break;
      case "confirmed":
        value = "تم التأكيد";
        break;
      case "rejected":
        value = "مرفوض";
        break;
      case "delivered":
        value = "تم التوصيل";
        break;
      default:
        value = "قيد المعاينة";
        break;
    }
    return value;
  };

  handleSearchSubmit = (e) => {
    e.preventDefault();
    this.getData(this.state.itemStatus);
  };
  handleSearchChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
    let vm = this;
    setTimeout(function() {
      vm.getData(vm.state.itemStatus);
    }, 100);
  };

  render() {
    let renderItems = (
      <tr>
        <th scope="col" className="table-bordered ">
          ---
        </th>
        <th scope="col">---</th>
        <th scope="col">---</th>
        <th scope="col">---</th>
        <th scope="col">---</th>
        <th scope="col" className="text-center">
          ---
        </th>
      </tr>
    );
    if (this.state.items !== null) {
      renderItems = this.state.items.map((item, index) => {
        let editID = "editModal_" + item.id;
        let statuses = [
          "preview",
          "onway",
          "confirmed",
          "rejected",
          "delivered",
        ];

        const renderEditModal = (
          <div
            className="modal fade text-left"
            id={editID}
            tabIndex="-1"
            role="dialog"
            aria-labelledby="myModalLabel1"
            aria-hidden="true"
          >
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-body">
                  <h4 className="text-center mb-2">كافة البيانات</h4>
                  <form onSubmit={this.handleSubmitEdit(item)}>
                    <table className="table">
                      <tbody>
                        <tr>
                          <td>
                            <label htmlFor="">التصنيف</label>
                            <select
                              defaultValue={item.status}
                              onChange={this.handleEditChange(item)}
                              className="form-control"
                              name="status"
                              id=""
                            >
                              {statuses.map((st) => {
                                return (
                                  <option key={st} value={st}>
                                    {this.translateStatus(st)}
                                  </option>
                                );
                              })}
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <input
                              type="submit"
                              className="btn btn-success"
                              value="حفظ"
                            />
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </form>
                </div>
                <div className="modal-footer bg-flight">
                  <button
                    type="button"
                    className="btn grey btn-outline-light"
                    data-dismiss="modal"
                  >
                    <i className="la la-times-circle pr-1"></i>اغلاق
                  </button>
                </div>
              </div>
            </div>
          </div>
        );

        return (
          <tr key={item.id}>
            <th scope="row" className="table-bordered ">
              {index + 1}
            </th>
            <td>
              <span className="avatar avatar-sm rounded-circle mr-1">
                <img src={ImageBasePath + "/users/" + item.user.image} alt="" />
              </span>
              {item.user.name}
            </td>
            <td>{item.items.length}</td>
            <td>{item.total_cost + " ر.س"}</td>
            <td>{this.translateStatus(item.status)}</td>
            <td>{item.createdAt}</td>
            <td>
              <div className="float-md-right ">
                <Link
                  to={"/order/" + item.id}
                  type="button"
                  className="btn btn-icon btn-success  box-shadow-1 mr-1 mb-1"
                  title=""
                  data-original-title="مشاهده"
                >
                  <i className="la la-eye"></i>
                </Link>

                <button
                  type="button"
                  className="btn btn-icon btn-warning  box-shadow-1 mr-1 mb-1"
                  title=""
                  data-original-title="تعديل"
                  onClick={() => this.viewModal(editID)}
                >
                  <i className="la la-edit"></i>
                </button>
                {renderEditModal}
              </div>
            </td>
          </tr>
        );
      });
    }
    let statuses = ["preview", "confirmed", "onway", "delivered", "rejected"];
    return (
      <div className="app-content content">
        <div className="content-wrapper">
          <div className="content-body">
            <Breadcrumb routeName="طلبات العملاء" linkTo="/" />

            <div className="card">
              <div className="card-content">
                <section className="card  pt-0 pb-0">
                  <div className="card-content">
                    <div className="card-header">
                      <div className="float-left">
                        <h4 className="mb-0">
                          <strong>البحث</strong>
                        </h4>
                      </div>
                      <div className="float-right"></div>
                    </div>
                    <div className="card-body pt-1 pb-0">
                      <div className="card-text">
                        <form onSubmit={this.handleSearchSubmit}>
                          <div className="form-body">
                            <div className="row">
                              <div className="col-md-3">
                                <div className="form-group">
                                  <label>حالة الطلب</label>
                                  <select
                                    value={this.state.itemStatus}
                                    onChange={this.handleSearchChange}
                                    className="form-control success"
                                    name="itemStatus"
                                    id="itemStatus"
                                  >
                                    <option value="">كل الحالات</option>
                                    {statuses.map((itemSt) => {
                                      return (
                                        <option key={itemSt} value={itemSt}>
                                          {this.translateStatus(itemSt)}
                                        </option>
                                      );
                                    })}
                                  </select>
                                </div>
                              </div>
                              <div
                                className="col-md-1"
                                style={{ padding: "25px" }}
                              >
                                <button
                                  type="submit"
                                  className="btn btn-icon btn-light float-right box-shadow-1 mt-0 mb-0"
                                  data-toggle="tooltip"
                                  data-placement="top"
                                  title=""
                                  data-original-title="بحث"
                                >
                                  <i className="la la-filter"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>

            <section className="card">
              <div className="card-content">
                <div className="card-body">
                  <div className="card-text">
                    <div className="table-striped">
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col" className="table-bordered ">
                              #
                            </th>
                            <th scope="col">العميل</th>
                            <th scope="col">عدد المنتجات</th>
                            <th scope="col">التكلفة</th>
                            <th scope="col">حالة الطلب</th>
                            <th scope="col">تاريخ الطلب</th>
                            <th scope="col" className="text-center">
                              خيارات
                            </th>
                          </tr>
                        </thead>
                        <tbody>{renderItems}</tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}
export default Orders;
