import React, { Component } from "react";

import ProductCard from "./../../components/ProductCard";
import { AxiosCall } from "./../../helpers/Shared";
import Breadcrumb from "./../../components/Breadcrumb";

class Products extends Component {
    state = {
        keyword: "",
        category_id: 0,
        categoryID: 0,
        categories: [],
        items: []
    };

    getData = (key = null, categoryID = null) => {
        let endPoint = `get-products`;
        endPoint += key === null ? "?key=" : `?key=${key}`;
        endPoint +=
            categoryID === null
                ? "&category_id="
                : `&category_id=${categoryID}`;

        let response = AxiosCall(endPoint, "GET");
        response
            .then(result => result.data)
            .then(data => data.data)
            .then(data => {
                //console.log('Get Data 2', data)
                this.setState({
                    isLoading: false,
                    admin: data.admin,
                    perPage: data.perPage,
                    items: data.perPage > 0 ? data.items.data : data.items,
                    last_page: data.perPage > 0 ? data.items.last_page : "",
                    next_page_url:
                        data.perPage > 0 ? data.items.next_page_url : "",
                    prev_page_url:
                        data.perPage > 0 ? data.items.prev_page_url : "",
                    first_page_url:
                        data.perPage > 0 ? data.items.first_page_url : "",
                    last_page_url:
                        data.perPage > 0 ? data.items.last_page_url : "",
                    page: data.page
                });
            });
    };

    getCategories = () => {
        let endPoint = "get-categories?perPage=0";
        let response = AxiosCall(endPoint, "GET");
        response
            .then(result => result.data)
            .then(data => data.data)
            .then(data => {
                this.setState({
                    categories: data.items
                });
            });
    };

    handleSearchSubmit = e => {
        e.preventDefault();
        this.getData(this.state.keyword, this.state.category_id);
    };
    handleSearchChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        let vm = this;
        if (e.target.name !== "category_id") {
            setTimeout(function() {
                vm.getData(vm.state.keyword, vm.state.category_id);
            }, 100);
        }
    };

    deleteModal = (id, modalID) => {
        window.$("#" + modalID).modal("hide");
        console.log(id);
        let endPoint = "delete-product/" + id;
        let res = AxiosCall(endPoint, "DELETE");
        res.then(
            response => {
                alert(response.data.message);
                console.log(response);
            },
            error => {
                console.log(error.response.data.messagee);
            }
        );
        this.setState({
            items: this.state.items.filter(item => item.id !== id)
        });
    };

    componentWillMount() {
        this.getData();
        this.getCategories();
    }

    render() {
        let renderItems = this.state.items.map(item => {
            return (
                <ProductCard
                    deleteModal={this.deleteModal}
                    key={item.id}
                    productID={item.id}
                    name={item.name}
                    image={item.image}
                    price={item.price}
                    netPrice={item.net_price}
                    discount={item.discount}
                    category={item.category["name"]}
                    cName="col-xl-2 col-lg-3 col-md-3 col-sm-3"
                    //cName="col-xl-4 col-lg-6 col-md-6 col-sm-6"
                />
            );
        });

        return (
            <div className="app-content content">
                <div className="content-wrapper">
                    <div className="content-detached">
                        <div className="content-body">
                            <Breadcrumb routeName="المنتجات" linkTo="/" />

                            <div className="card">
                                <div className="card-content">
                                    <section className="card  pt-0 pb-0">
                                        <div className="card-content">
                                            <div className="card-header">
                                                <div className="float-left">
                                                    <h4 className="mb-0">
                                                        <strong>البحث</strong>
                                                    </h4>
                                                </div>
                                                <div className="float-right"></div>
                                            </div>
                                            <div className="card-body pt-1 pb-0">
                                                <div className="card-text">
                                                    <form
                                                        onSubmit={
                                                            this
                                                                .handleSearchSubmit
                                                        }
                                                    >
                                                        <div className="form-body">
                                                            <div className="row">
                                                                <div className="col-md-4 pr-0">
                                                                    <div className="form-group">
                                                                        <label>
                                                                            اسم
                                                                            المنتج
                                                                        </label>
                                                                        <input
                                                                            onChange={
                                                                                this
                                                                                    .handleSearchChange
                                                                            }
                                                                            type="text"
                                                                            className="form-control"
                                                                            placeholder="بحث باسم المنتج"
                                                                            name="keyword"
                                                                            value={
                                                                                this
                                                                                    .state
                                                                                    .keyword
                                                                            }
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div className="col-md-3">
                                                                    <div className="form-group">
                                                                        <label>
                                                                            التصنيف
                                                                        </label>
                                                                        <select
                                                                            value={
                                                                                this
                                                                                    .state
                                                                                    .category_id
                                                                            }
                                                                            onChange={
                                                                                this
                                                                                    .handleSearchChange
                                                                            }
                                                                            className="form-control success"
                                                                            name="category_id"
                                                                            id="category_id"
                                                                        >
                                                                            <option value="0">
                                                                                اختر
                                                                                التصنيف
                                                                            </option>
                                                                            {this.state.categories.map(
                                                                                category => {
                                                                                    return (
                                                                                        <option
                                                                                            key={
                                                                                                category.id
                                                                                            }
                                                                                            value={
                                                                                                category.id
                                                                                            }
                                                                                        >
                                                                                            {
                                                                                                category.name
                                                                                            }
                                                                                        </option>
                                                                                    );
                                                                                }
                                                                            )}
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div
                                                                    className="col-md-1"
                                                                    style={{
                                                                        padding:
                                                                            "25px"
                                                                    }}
                                                                >
                                                                    <button
                                                                        type="submit"
                                                                        className="btn btn-icon btn-light float-right box-shadow-1 mt-0 mb-0"
                                                                        data-toggle="tooltip"
                                                                        data-placement="top"
                                                                        title=""
                                                                        data-original-title="بحث"
                                                                    >
                                                                        <i className="la la-filter"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>

                            <div className="product-shop">
                                <div className="row match-height">
                                    {renderItems}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Products;
