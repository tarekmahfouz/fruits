import React, { Component } from "react";

import { AxiosCall, SendPostRequest } from "../../helpers/Shared";
import { v1 as uuidv1 } from "uuid";

class EditProduct extends Component {
  state = {
    id: this.props.match.params.id ?? null,
    name: "",
    description: "",
    image: null,
    category_id: "",
    category_name: "",
    productWeights: [],
    weights: [],
    categories: [],
  };

  getData = () => {
    let endPoint = "get-product/" + this.state.id;
    let response = AxiosCall(endPoint, "GET");
    response
      .then((result) => result.data)
      .then((data) => data.data)
      .then((data) => {
        this.setState({
          name: data.name,
          description: data.description,
          productWeights: data.weight_units,
          category_id: data.category.id,
          category_name: data.category.name,
        });
      });
    //console.log(this.state);
  };

  getWeights = () => {
    let endPoint = "get-weights";
    let response = AxiosCall(endPoint, "GET");
    response
      .then((result) => result.data)
      .then((data) => data.data)
      .then((data) => {
        this.setState({
          weights: data.items,
        });
      });
  };

  appendPriceRow = () => {
    let productWeights = [
      ...this.state.productWeights,
      {
        id: uuidv1(),
        weight_id: this.state.weights[0].id,
        weight_price: 1,
        start_from: 1,
      },
    ];
    this.setState({ productWeights });
  };

  deleteProductPrice = (id) => {
    let productWeights = this.state.productWeights.filter(
      (item) => item.id !== id
    );
    this.setState({ productWeights });
    /* let endPoint = "delete-product-price/" + id;
        let response = AxiosCall(endPoint, "DELETE");
        response
            .then(result => result.data)
            .then(data => data.data)
            .then(data => {
                let productWeights = this.state.productWeights.filter(
                    item => item.id !== id
                );
                this.setState({ productWeights });
            }); */
  };

  getCategories = () => {
    let endPoint = "get-categories?perPage=0";
    let response = AxiosCall(endPoint, "GET");
    response
      .then((result) => result.data)
      .then((data) => data.data)
      .then((data) => {
        this.setState({
          categories: data.items,
        });
      });
  };

  componentDidMount() {
    this.getData();
    this.getWeights();
    this.getCategories();
  }

  handleChange = (e) => {
    if (e.target.name !== "image") {
      this.setState({
        [e.target.name]: e.target.value,
      });
    } else {
      this.setState({
        image: e.target.files[0],
      });
    }
  };

  handleWeightChange = (weightItem) => (e) => {
    console.log(e);
    if (e.target.name === "weight_" + weightItem.id) {
      weightItem.weight_id = e.target.value;
    } else if (e.target.name === "start_from_" + weightItem.id) {
      weightItem.start_from = e.target.value;
    } else if (e.target.name === "price_" + weightItem.id) {
      weightItem.weight_price = e.target.value;
    }

    this.setState((state) => {
      const productWeights = state.productWeights.map((item) => {
        if (item.id === weightItem.id) {
          return weightItem;
        } else {
          return item;
        }
      });
      return {
        productWeights,
      };
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    let endPoint = "edit-product/" + this.state.id;
    let formData = [
      { key: "name", value: this.state.name },
      { key: "description", value: this.state.description },
      { key: "category_id", value: this.state.category_id },
      {
        key: "weights",
        value: JSON.stringify(this.state.productWeights),
      },
      { key: "_method", value: "PUT" },
    ];
    if (this.state.image != null) {
      formData.push({ key: "image", value: this.state.image });
    }
    let headers = { "content-type": "multipart/form-data" };
    let res = SendPostRequest(endPoint, formData, headers);

    res.then(
      (response) => {
        alert(response.data.message);
        this.getData();
      },
      (error) => {
        alert(error.response.data.message);
        console.log(error.response.data.message);
      }
    );
  };

  render() {
    //let selectStatus = !status ? 'selected' : '';
    let { name, description, weights } = this.state;

    let renderproductWeights = this.state.productWeights.map((itemWeight) => {
      let options = weights.map((option) => {
        let selected = itemWeight.weight_id === option.id ? true : false;
        return (
          <option key={option.id} selected={selected} value={option.id}>
            {option.name}
          </option>
        );
      });
      return (
        <div key={itemWeight.id} className="row">
          <div className="col-md-4">
            <div className="form-group">
              <label>الوحدة أو الوزن</label>
              <select
                type="text"
                className="form-control success"
                name={"weight_" + itemWeight.id}
                onChange={this.handleWeightChange(itemWeight)}
              >
                {options}
              </select>
            </div>
          </div>

          <div className="col-md-3">
            <div className="form-group">
              <label>البيع للمنتج يبدأ من</label>
              <input
                type="number"
                step="0.1"
                className="form-control success"
                placeholder="البيع للمنتج يبدأ من"
                name={"start_from_" + itemWeight.id}
                value={itemWeight.start_from}
                onChange={this.handleWeightChange(itemWeight)}
              />
            </div>
          </div>

          <div className="col-md-4">
            <div className="form-group">
              <label>سعر البيع للوحدة أو الوزن المختار</label>
              <input
                type="number"
                step="0.1"
                className="form-control success"
                placeholder="سعر البيع للوحدة أو الوزن المختار"
                name={"price_" + itemWeight.id}
                value={itemWeight.weight_price}
                onChange={this.handleWeightChange(itemWeight)}
              />
            </div>
          </div>

          <div className="col-md-1" style={{ padding: "30px" }}>
            <div className="form-group">
              <i
                className="la la-trash"
                style={{ color: "red", cursor: "pointer" }}
                onClick={() => this.deleteProductPrice(itemWeight.id)}
              ></i>
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className="app-content content">
        <div className="content-wrapper">
          <div className="content-body">
            <section className="card">
              <div className="card-content">
                <div className="card-body">
                  <div className="card-text">
                    <form onSubmit={this.handleSubmit}>
                      <div className="form-body">
                        <h4 className="form-section">
                          <i className="la la-caret-square-o-left"></i>
                          تفاصيل المنتج
                        </h4>

                        <div className="row">
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>الاسم</label>
                              <input
                                type="text"
                                className="form-control success"
                                placeholder="name"
                                name="name"
                                value={name}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                          <div className="col-md-6">
                            <div className="form-group">
                              <label>التصنيف</label>
                              <select
                                value={this.state.category_id}
                                onChange={this.handleChange}
                                className="form-control success"
                                name="category_id"
                                id="category_id"
                              >
                                <option value="0">اختر التصنيف</option>
                                {this.state.categories.map((mainCat) => {
                                  return (
                                    <option key={mainCat.id} value={mainCat.id}>
                                      {mainCat.name}
                                    </option>
                                  );
                                })}
                              </select>
                            </div>
                          </div>

                          <div className="col-md-12">
                            <div className="form-group">
                              <label>الوصف</label>
                              <textarea
                                value={description}
                                className="form-control success"
                                placeholder="الوصف"
                                name="description"
                                rows="5"
                                onChange={this.handleChange}
                              ></textarea>
                            </div>
                          </div>
                          <div className="col-md-12">
                            <div className="form-group">
                              <label>اختر صورة</label>
                              <input
                                name="image"
                                type="file"
                                className="form-control"
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                        </div>

                        <hr />

                        <fieldset
                          style={{
                            padding: "10px",
                            borderWidth: "1px",
                            borderStyle: "solid",
                            borderColor: "grey !important",
                            borderRadius: "10px",
                          }}
                        >
                          <legend>
                            الأسعار &nbsp;{" "}
                            <i
                              className="la la-plus"
                              style={{
                                color: "green",
                                cursor: "pointer",
                              }}
                              onClick={this.appendPriceRow}
                            ></i>
                          </legend>
                          <div
                            className="col-md-12"
                            id="productWeightsContainer"
                          >
                            {renderproductWeights}
                          </div>
                        </fieldset>
                      </div>

                      <div className="form-actions right  ">
                        <button type="submit" className="btn btn-success">
                          <i className="la la-check-square-o pr-1"></i>
                          حفظ
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default EditProduct;
