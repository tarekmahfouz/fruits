import React, { Component, useContext } from "react";
import { AxiosCall, SendPostRequest } from "../../helpers/Shared";

import Breadcrumb from "./../../components/Breadcrumb";

class Inbox extends Component {
    state = {
        isLoading: true,
        name: "",
        email: "",
        message: "",
        items: null,
        perPage: 10,
        page: 0,
        last_page: "",
        first_page_url: "",
        last_page_url: "",
        next_page_url: "",
        prev_page_url: ""
    };

    getData = (filters = null) => {
        let endPoint = "get-inbox";
        /* if(filters !== null) {
            if('parentID' in filters) {
                this.setState({
                    parent_id: filters.parentID
                });
                endPoint = "get-categories?parent_id="+filters.parentID;
            } else {
                this.setState({
                    parent_id: null
                });
            }
            if('keyword' in filters) {
                endPoint = "get-categories?key="+filters.keyword;
            }
        } */

        let response = AxiosCall(endPoint, "GET");
        response
            .then(result => result.data)
            .then(data => data.data)
            .then(data => {
                //console.log('Get Data 2', data)
                this.setState({
                    isLoading: false,
                    perPage: data.perPage,
                    items: data.perPage > 0 ? data.items.data : data.items,
                    last_page: data.perPage > 0 ? data.items.last_page : "",
                    next_page_url:
                        data.perPage > 0 ? data.items.next_page_url : "",
                    prev_page_url:
                        data.perPage > 0 ? data.items.prev_page_url : "",
                    first_page_url:
                        data.perPage > 0 ? data.items.first_page_url : "",
                    last_page_url:
                        data.perPage > 0 ? data.items.last_page_url : "",
                    page: data.page
                });
            });
    };

    componentWillMount() {
        this.getData();
    }

    viewModal = viewID => {
        window.$("#" + viewID).modal("show");
    };

    deleteModal = async (id, modalID) => {
        window.$("#" + modalID).modal("hide");
        let endPoint = "delete-message/" + id;
        await AxiosCall(endPoint, "DELETE");
        this.setState({
            items: this.state.items.filter(item => item.id !== id)
        });
    };

    render() {
        let renderItems = (
            <tr>
                <th scope="col" className="table-bordered ">
                    ---
                </th>
                <th scope="col">---</th>
                <th scope="col">---</th>
                <th scope="col">---</th>
                <th scope="col" className="text-center">
                    ---
                </th>
            </tr>
        );
        if (this.state.items !== null) {
            renderItems = this.state.items.map((item, index) => {
                let viewID = "viewModal_" + item.id;
                let deleteID = "deleteModal_" + item.id;

                const renderViewModal = (
                    <div
                        className="modal fade text-left"
                        id={viewID}
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel1"
                        aria-hidden="true"
                    >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <h4 className="text-center mb-2">
                                        بيانات الرسالة
                                    </h4>
                                    <table className="table">
                                        <tbody>
                                            <tr>
                                                <td>الاسم</td>
                                                <td> {item.name} </td>
                                            </tr>
                                            <tr>
                                                <td> رقم الجوال </td>
                                                <td> {item.phone} </td>
                                            </tr>
                                            <tr>
                                                <td> الرسالة </td>
                                                <td> {item.message} </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="modal-footer bg-flight">
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-light"
                                        data-dismiss="modal"
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        اغلاق
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                const renderDeleteModal = (
                    <div
                        className="modal fade text-left"
                        id={deleteID}
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel1"
                        aria-hidden="true"
                    >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <h5 className="text-center mb-2">
                                        {" "}
                                        هل تريد اتمام الحذف؟
                                    </h5>
                                </div>
                                <div className="modal-footer bg-flight">
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-danger"
                                        onClick={() =>
                                            this.deleteModal(item.id, deleteID)
                                        }
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        حذف
                                    </button>
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-light"
                                        data-dismiss="modal"
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        اغلاق
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                );
                /* 
                <button type="button" className="btn btn-icon  btn-danger  box-shadow-1 mr-1 mb-1" title="" data-original-title="حذف" onClick={ () => this.viewModal(deleteID)}><i className="la la-trash"></i></button>
                {renderDeleteModal}
                */

                return (
                    <tr key={item.id}>
                        <th scope="row" className="table-bordered ">
                            {index + 1}
                        </th>
                        <td> {item.name} </td>
                        <td> {item.phone} </td>
                        <td> {item.message.slice(0, 100) + "..."} </td>
                        <td>
                            <div className="float-md-right ">
                                <button
                                    type="button"
                                    className="btn btn-icon btn-success  box-shadow-1 mr-1 mb-1"
                                    title=""
                                    data-original-title="مشاهده"
                                    onClick={() => this.viewModal(viewID)}
                                >
                                    <i className="la la-eye"></i>
                                </button>
                                {renderViewModal}
                            </div>
                            <div className="float-md-right ">
                                <button
                                    type="button"
                                    className="btn btn-icon btn-danger  box-shadow-1 mr-1 mb-1"
                                    title=""
                                    data-original-title="حذف"
                                    onClick={() => this.viewModal(deleteID)}
                                >
                                    <i className="la la-trash"></i>
                                </button>
                                {renderDeleteModal}
                            </div>
                        </td>
                    </tr>
                );
            });
        }

        return (
            <div className="app-content content">
                <div className="content-wrapper">
                    <div className="content-body">
                        <Breadcrumb routeName="الرسائل الواردة" linkTo="/" />

                        <section className="card">
                            <div className="card-content">
                                <div className="card-body">
                                    <div className="card-text">
                                        <div className="table-striped">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th
                                                            scope="col"
                                                            className="table-bordered "
                                                        >
                                                            #
                                                        </th>
                                                        <th scope="col">
                                                            الاسم
                                                        </th>
                                                        <th scope="col">
                                                            رقم الجوال
                                                        </th>
                                                        <th scope="col">
                                                            نص الرسالة
                                                        </th>
                                                        <th
                                                            scope="col"
                                                            className="text-center"
                                                        >
                                                            الخيارات
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>{renderItems}</tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }
}
export default Inbox;
