import React, { Component, useContext } from "react";
import { AxiosCall, SendPostRequest } from "../../helpers/Shared";

class Config extends Component {
    state = {
        isLoading: true,
        name: "",
        shipping_value: 0,
        whatsapp: "",
        phone: "",
        about_us: ""
    };

    getData = (filters = null) => {
        let configEndPoint = "get-config";

        let configResponse = AxiosCall(configEndPoint, "GET");
        configResponse
            .then(result => result.data)
            .then(data => data.data)
            .then(data => {
                this.setState({
                    isLoading: false,
                    shipping_value: data.items.shipping_value,
                    phone: data.items.phone,
                    whatsapp: data.items.whatsapp,
                    about_us: data.items.about_us
                });
            });
    };

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        //console.log(this.state);
    };
    handleSubmit = e => {
        e.preventDefault();
        let configEndPoint = "edit-config";
        let res = "";
        let data = {};
        let shipping_value = parseFloat(this.state.shipping_value);
        data = [
            {
                key: "shipping_value",
                value: Number.isNaN(shipping_value) ? 0 : shipping_value
            },
            { key: "phone", value: this.state.phone },
            { key: "whatsapp", value: this.state.whatsapp },
            { key: "about_us", value: this.state.about_us },
            { key: "_method", value: "PUT" }
        ];
        res = SendPostRequest(configEndPoint, data);
        res.then(
            response => {
                alert(response.data.message);
                //console.log(response);
            },
            error => {
                alert(error.response.message);
                //console.log(error.response);
            }
        );
        //console.log(data);
    };
    componentWillMount() {
        this.getData();
    }

    render() {
        let { tax, shipping_value, whatsapp, phone, about_us } = this.state;
        return (
            <div className="app-content content">
                <div className="content-wrapper">
                    <div className="content-body">
                        <section className="card">
                            <div className=" row">
                                <div className="col-12">
                                    <div className="card-content collapse show">
                                        <div className="card-body">
                                            <form onSubmit={this.handleSubmit}>
                                                <div className="form-body">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <label>
                                                                    <i className="la la-mobile mr-1">
                                                                        {" "}
                                                                    </i>
                                                                    رقم الجوال
                                                                </label>
                                                                <input
                                                                    name="phone"
                                                                    value={
                                                                        phone
                                                                    }
                                                                    type="text"
                                                                    className="form-control success"
                                                                    placeholder="رقم الجوال"
                                                                    onChange={
                                                                        this
                                                                            .handleChange
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <label>
                                                                    <i className="la la-whatsapp mr-1">
                                                                        {" "}
                                                                    </i>
                                                                    الواتس اب
                                                                </label>
                                                                <input
                                                                    name="whatsapp"
                                                                    value={
                                                                        whatsapp
                                                                    }
                                                                    type="text"
                                                                    className="form-control"
                                                                    placeholder="الواتس اب"
                                                                    onChange={
                                                                        this
                                                                            .handleChange
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <label>
                                                                    <i className="la la-calculator mr-1">
                                                                        {" "}
                                                                    </i>
                                                                    التوصيل
                                                                </label>
                                                                <input
                                                                    name="shipping_value"
                                                                    value={
                                                                        shipping_value
                                                                    }
                                                                    type="text"
                                                                    className="form-control"
                                                                    placeholder="يتم حساب  قيمة التوصيل بالنسبة المئوية"
                                                                    onChange={
                                                                        this
                                                                            .handleChange
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className=" float-md-right mb-1">
                                                        <button
                                                            type="submit"
                                                            className="btn btn-warning right"
                                                        >
                                                            <i className="la la-check-square-o pr-1"></i>{" "}
                                                            حفظ
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section className="card">
                            <div className=" row">
                                <div className="col-12">
                                    <div className="card-content collapse show">
                                        <div className="card-body">
                                            <form onSubmit={this.handleSubmit}>
                                                <div className="form-body">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <label htmlFor="about_us">
                                                                    <i className="la la-edit mr-1">
                                                                        {" "}
                                                                    </i>
                                                                    من نحن
                                                                </label>
                                                                <textarea
                                                                    name="about_us"
                                                                    id="about_us"
                                                                    rows="10"
                                                                    className="form-control success"
                                                                    placeholder="من نحن"
                                                                    onChange={
                                                                        this
                                                                            .handleChange
                                                                    }
                                                                    value={
                                                                        about_us
                                                                    }
                                                                ></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className=" float-md-right mb-1">
                                                        <button
                                                            type="submit"
                                                            className="btn btn-warning right"
                                                        >
                                                            <i className="la la-check-square-o pr-1"></i>{" "}
                                                            حفظ
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }
}
export default Config;
