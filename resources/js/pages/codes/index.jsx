import React, { Component, useContext } from "react";
import {
    AxiosCall,
    ImageBasePath,
    SendPostRequest
} from "../../helpers/Shared";
import Breadcrumb from "../../components/Breadcrumb";

class Codes extends Component {
    state = {
        code: "",
        value: "",
        status: 1,
        image: null,
        notification_content: "عزيزى العميل يمكنك استخدام هذا الكود للخصومات: ",
        selected_users: [],
        isLoading: true,
        mainCategroies: [],
        admin: "",
        items: null,
        users: [],
        perPage: 10,
        page: 0,
        last_page: "",
        first_page_url: "",
        last_page_url: "",
        next_page_url: "",
        prev_page_url: "",
    };

    getUsers = () => {
        let endPoint = `get-users`;
        let response = AxiosCall(endPoint, "GET");
        response
            .then(result => result.data)
            .then(data => data.data)
            .then(data => {
                this.setState({
                    users: data.perPage > 0 ? data.items.data : data.items
                });
            });
    };

    getData = (filters = null) => {
        let endPoint = "get-codes";
        if (filters !== null) {
            if ("keyword" in filters) {
                endPoint = "get-codes?key=" + filters.keyword;
            }
        }

        let response = AxiosCall(endPoint, "GET");
        response
            .then(result => result.data)
            .then(data => data.data)
            .then(data => {
                //console.log('Get Data 2', data)
                this.setState({
                    isLoading: false,
                    admin: data.admin,
                    perPage: data.perPage,
                    items: data.perPage > 0 ? data.items.data : data.items,
                    last_page: data.perPage > 0 ? data.items.last_page : "",
                    next_page_url:
                        data.perPage > 0 ? data.items.next_page_url : "",
                    prev_page_url:
                        data.perPage > 0 ? data.items.prev_page_url : "",
                    first_page_url:
                        data.perPage > 0 ? data.items.first_page_url : "",
                    last_page_url:
                        data.perPage > 0 ? data.items.last_page_url : "",
                    page: data.page
                });
            });
    };

    componentWillMount() {
        this.getData();
        this.getUsers();
    }

    viewModal = (e, viewID) => {
        if (e.target.name === "sendNotification") {
            let message =
                "عزيزى العميل يمكنك استخدام هذا الكود للخصومات " + e.target.id;

            this.setState({
                notification_content: message
            });
        }
        window.$("#" + viewID).modal("show");
    };
    viewSendModal = (code, viewID) => {
        let message = "عزيزى العميل يمكنك استخدام هذا الكود للخصومات: " + code;
        this.setState({
            notification_content: message
        });
        window.$("#" + viewID).modal("show");
    };
    dismissModal = viewID => {
        this.setState({
            selected_users: [],
            notification_content:
                "عزيزى العميل يمكنك استخدام هذا الكود للخصومات: "
        });
        window.$("#" + viewID).modal("hide");
    };

    deleteModal = async (id, modalID) => {
        window.$("#" + modalID).modal("hide");
        let endPoint = "delete-code/" + id;
        await AxiosCall(endPoint, "DELETE");
        this.setState({
            items: this.state.items.filter(item => item.id !== id)
        });
    };

    handleChange = e => {
        e.preventDefault();
        if (e.target.name === "notification_content") {
            this.setState({
                [e.target.name]: e.target.value
            });
        } else {
            let options = e.target.options;
            let selected_users = [];
            for (var i = 0; i < options.length; i++) {
                if (options[i].selected) {
                    selected_users.push(options[i].value);
                }
            }
            this.setState({ selected_users });
        }
    };

    handleEditChange = toUpdateItem => e => {
        /* var items = this.state.items;
        var toUpdateItem = items.findIndex(function(c) { 
            return c.id == item.id; 
        }); */
        if (e.target.name == "editCode") {
            toUpdateItem.code = e.target.value;
            if (toUpdateItem.newImage == null) toUpdateItem.newImage = null;
        }
        if (e.target.name == "editValue") {
            toUpdateItem.value = e.target.value;
            if (toUpdateItem.newImage == null) toUpdateItem.newImage = null;
        }
        if (e.target.name == "editStatus") {
            toUpdateItem.status = e.target.value;
            if (toUpdateItem.newImage == null) toUpdateItem.newImage = null;
        }
        if (e.target.name == "editImage") {
            toUpdateItem.newImage = e.target.files[0];
        }
        //console.log(toUpdateItem);
        if (this.state.items.length > 0) {
            this.setState(state => {
                const items = state.items.map(item => {
                    if (item.id === toUpdateItem.id) {
                        return toUpdateItem;
                    } else {
                        return item;
                    }
                });
                return {
                    items
                };
            });
        }
    };

    handleSubmitEdit = item => e => {
        e.preventDefault();
        let endPoint = "edit-code/" + item.id;
        let headers = { "content-type": "multipart/form-data" };
        let data = [
            { key: "_method", value: "PUT" },
            { key: "code", value: item.code },
            { key: "value", value: item.value },
            { key: "status", value: item.status }
        ];
        console.log(data);
        if (item.newImage !== null) {
            data.push({ key: "image", value: item.newImage });
        }
        let res = SendPostRequest(endPoint, data, headers);
        res.then(
            response => {
                alert(response.data.message);
                console.log(response);
                window.$("#editModal_" + item.id).modal("hide");
            },
            error => {
                alert(error.response.message);
                console.log(error.response);
            }
        );
    };

    handleSubmitSendNotification = e => {
        e.preventDefault();
        let endPoint = "send-notifications";
        let formData = [
            {
                key: "notification_content",
                value: this.state.notification_content
            },
            {
                key: "selected_users",
                value: JSON.stringify(this.state.selected_users)
            }
        ];

        let res = SendPostRequest(endPoint, formData);

        res.then(
            response => {
                alert(response.data.message);
                this.setState({ selected_users: [] });
            },
            error => {
                alert(error.response.data.message);
            }
        );
    };

    handleAddChange = e => {
        if (e.target.name == "code") {
            this.setState({
                code: e.target.value
            });
        }
        if (e.target.name == "value") {
            this.setState({
                value: e.target.value
            });
        }
        if (e.target.name == "status") {
            this.setState({
                status: e.target.value
            });
        }
        if (e.target.name == "image") {
            this.setState({
                image: e.target.files[0]
            });
        }
    };

    handleSubmitAdd = e => {
        e.preventDefault();
        let endPoint = "add-code";
        let headers = { "content-type": "multipart/form-data" };
        let data = [
            { key: "code", value: this.state.code },
            { key: "value", value: this.state.value },
            { key: "status", value: this.state.status }
        ];
        if (this.state.image !== null) {
            data.push({ key: "image", value: this.state.image });
        }
        let res = SendPostRequest(endPoint, data, headers);
        res.then(
            response => {
                let newItems = [...this.state.items];
                let itemToAdd = response.data.data;
                newItems.unshift(itemToAdd);
                this.setState({
                    items: newItems,
                    code: "",
                    value: "",
                    status: 1,
                    image: null
                });
                window.$("#addModal").modal("hide");
            },
            error => {
                //alert(error.response.data.message);
                alert(error.response.statusText);
                console.log(error.response);
            }
        );
    };

    render() {

        let renderItems = (
            <tr>
                <th scope="col" className="table-bordered ">
                    ---
                </th>
                <th scope="col">---</th>
                <th scope="col">---</th>
                <th scope="col">---</th>
                <th scope="col" className="text-center">
                    ---
                </th>
            </tr>
        );
        if (this.state.items !== null) {
            renderItems = this.state.items.map((item, index) => {
                let status = item.status
                    ? "badge badge-success"
                    : "badge badge-danger";
                let thumb = item.status
                    ? "la la-thumbs-o-up"
                    : "la la-thumbs-o-down";
                let isActive = item.status ? "مفعل" : "غير مفعل";

                let viewID = "viewModal_" + item.id;
                let editID = "editModal_" + item.id;
                let deleteID = "deleteModal_" + item.id;
                let sendModalID = "sendModalID_" + item.id;

                let renderUsersAsOptions = this.state.users.map(item => {
                    return (
                        <option key={item.id} value={item.id}>
                            {item.name}
                        </option>
                    );
                });
                const renderSendNotificationModal = (
                    <div
                        className="modal fade text-left"
                        id={sendModalID}
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel1"
                        aria-hidden="true"
                    >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <h4 className="text-center mb-2">
                                        كافة البيانات
                                    </h4>
                                    <form
                                        onSubmit={
                                            this.handleSubmitSendNotification
                                        }
                                    >
                                        <table className="table">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label htmlFor="">
                                                            نص الرسالة
                                                        </label>
                                                        <textarea
                                                            required
                                                            rows="5"
                                                            name="notification_content"
                                                            className="form-control"
                                                            onChange={
                                                                this
                                                                    .handleChange
                                                            }
                                                            value={
                                                                this.state
                                                                    .notification_content
                                                            }
                                                        ></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label htmlFor="">
                                                            الأعضاء
                                                        </label>
                                                        <select
                                                            required
                                                            multiple
                                                            className="form-control"
                                                            onChange={
                                                                this
                                                                    .handleChange
                                                            }
                                                            name="selected_users"
                                                            id=""
                                                        >
                                                            <option value="0">
                                                                كل الأعضاء
                                                            </option>
                                                            {
                                                                renderUsersAsOptions
                                                            }
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input
                                                            type="submit"
                                                            className="btn btn-success"
                                                            value="ارسال"
                                                        />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div className="modal-footer bg-flight">
                                    <button
                                        onClick={() =>
                                            this.dismissModal(sendModalID)
                                        }
                                        type="button"
                                        className="btn grey btn-outline-light"
                                        data-dismiss="modal"
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        اغلاق
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                const renderViewModal = (
                    <div
                        className="modal fade text-left"
                        id={viewID}
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel1"
                        aria-hidden="true"
                    >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <h4 className="text-center mb-2">
                                        البيانات
                                    </h4>
                                    <table className="table">
                                        <tbody>
                                            <tr>
                                                <td>الكود</td>
                                                <td> {item.code} </td>
                                            </tr>
                                            <tr>
                                                <td>القيمة</td>
                                                <td> {item.value} </td>
                                            </tr>
                                            <tr>
                                                <td>الحالة</td>
                                                <td>
                                                    <span
                                                        className={status}
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title=""
                                                        data-original-title={
                                                            isActive
                                                        }
                                                    >
                                                        <i
                                                            className={thumb}
                                                        ></i>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="modal-footer bg-flight">
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-light"
                                        data-dismiss="modal"
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        اغلاق
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                const renderEditModal = (
                    <div
                        className="modal fade text-left"
                        id={editID}
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel1"
                        aria-hidden="true"
                    >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <h4 className="text-center mb-2">
                                        كافة البيانات
                                    </h4>
                                    <form
                                        onSubmit={this.handleSubmitEdit(item)}
                                    >
                                        <table className="table">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label htmlFor="">
                                                            الكود
                                                        </label>
                                                        <input
                                                            type="text"
                                                            name="editCode"
                                                            className="form-control"
                                                            onChange={this.handleEditChange(
                                                                item
                                                            )}
                                                            value={item.code}
                                                        />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label htmlFor="">
                                                            القيمة
                                                        </label>
                                                        <input
                                                            type="text"
                                                            name="editValue"
                                                            className="form-control"
                                                            onChange={this.handleEditChange(
                                                                item
                                                            )}
                                                            value={item.value}
                                                        />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label htmlFor="">
                                                            الحالة
                                                        </label>
                                                        <select
                                                            value={status}
                                                            name="editStatus"
                                                            className="form-control"
                                                            onChange={this.handleEditChange(
                                                                item
                                                            )}
                                                            value={item.status}
                                                        >
                                                            <option value={1}>
                                                                مفعل
                                                            </option>
                                                            <option value={0}>
                                                                غير مفعل
                                                            </option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                {/* <tr>
                                                    <td>
                                                        <label htmlFor="">الصورة</label>
                                                        <input type="file" name="editImage" className="form-control" onChange={this.handleEditChange(item)}/>
                                                    </td>
                                                </tr> */}
                                                <tr>
                                                    <td>
                                                        <input
                                                            type="submit"
                                                            className="btn btn-success"
                                                            value="حفظ"
                                                        />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div className="modal-footer bg-flight">
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-light"
                                        data-dismiss="modal"
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        اغلاق
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                const renderDeleteModal = (
                    <div
                        className="modal fade text-left"
                        id={deleteID}
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel1"
                        aria-hidden="true"
                    >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <h5 className="text-center mb-2">
                                        {" "}
                                        هل تريد اتمام الحذف؟
                                    </h5>
                                </div>
                                <div className="modal-footer bg-flight">
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-danger"
                                        onClick={() =>
                                            this.deleteModal(item.id, deleteID)
                                        }
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        حذف
                                    </button>
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-light"
                                        data-dismiss="modal"
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        اغلاق
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                return (
                    <tr key={item.id}>
                        <th scope="row" className="table-bordered ">
                            {index + 1}
                        </th>
                        <td> {item.code} </td>
                        <td> {item.value} </td>
                        <td>
                            <span
                                className={status}
                                data-toggle="tooltip"
                                data-placement="top"
                                title=""
                                data-original-title={isActive}
                            >
                                <i className={thumb}></i>
                            </span>
                        </td>
                        <td>
                            <div className="float-md-right ">
                                <button
                                    type="button"
                                    className="btn btn-icon btn-success  box-shadow-1 mr-1 mb-1"
                                    title=""
                                    data-original-title="مشاهده"
                                    onClick={e => this.viewModal(e, viewID)}
                                >
                                    <i className="la la-eye"></i>
                                </button>
                                {renderViewModal}

                                <button
                                        type="button"
                                        className="btn btn-icon btn-warning  box-shadow-1 mr-1 mb-1"
                                        title=""
                                        data-original-title="تعديل"
                                        onClick={e => this.viewModal(e, editID)}
                                    >
                                        <i className="la la-edit"></i>
                                    </button> 
                                {renderEditModal}

                                <button
                                        type="button"
                                        className="btn btn-icon  btn-danger  box-shadow-1 mr-1 mb-1"
                                        title=""
                                        data-original-title="حذف"
                                        onClick={e =>
                                            this.viewModal(e, deleteID)
                                        }
                                    >
                                        <i className="la la-trash"></i>
                                    </button>
                                {renderDeleteModal}

                                <button
                                    type="button"
                                    title="ارسال اشعارات للعملاء"
                                    className="btn btn-icon  btn-danger  box-shadow-1 mr-1 mb-1"
                                    title=""
                                    data-original-title="حذف"
                                    onClick={() =>
                                        this.viewSendModal(
                                            item.code,
                                            sendModalID
                                        )
                                    }
                                >
                                    <i className="la la-envelope"></i>
                                </button>
                                {renderSendNotificationModal}
                            </div>
                        </td>
                    </tr>
                );
            });
        }

        const renderAddModal = (
            <div
                className="modal fade text-left"
                id="addModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="myModalLabel1"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <h4 className="text-center mb-2">كافة البيانات</h4>
                            <form onSubmit={this.handleSubmitAdd}>
                                <table className="table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label htmlFor="">الكود</label>
                                                <input
                                                    type="text"
                                                    name="code"
                                                    className="form-control"
                                                    onChange={
                                                        this.handleAddChange
                                                    }
                                                    value={this.state.code}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label htmlFor="">القيمة</label>
                                                <input
                                                    type="text"
                                                    name="value"
                                                    className="form-control"
                                                    onChange={
                                                        this.handleAddChange
                                                    }
                                                    value={this.state.value}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label htmlFor="">الحالة</label>
                                                <select
                                                    value={status}
                                                    name="status"
                                                    className="form-control"
                                                    onChange={
                                                        this.handleAddChange
                                                    }
                                                    value={this.state.status}
                                                >
                                                    <option value={1}>
                                                        مفعل
                                                    </option>
                                                    <option value={0}>
                                                        غير مفعل
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        {/* <tr>
                                            <td>
                                                <label htmlFor="">الصورة</label>
                                                <input type="file" name="image" className="form-control" onChange={this.handleAddChange}/>
                                            </td>
                                        </tr> */}
                                        <tr>
                                            <td>
                                                <input
                                                    type="submit"
                                                    className="btn btn-success"
                                                    value="حفظ"
                                                />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <div className="modal-footer bg-flight">
                            <button
                                type="button"
                                className="btn grey btn-outline-light"
                                data-dismiss="modal"
                            >
                                <i className="la la-times-circle pr-1"></i>اغلاق
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
        return (
            <div className="app-content content">
                <div className="content-wrapper">
                    <div className="content-body">
                        <Breadcrumb routeName="الكود" linkTo="/" />

                        <button
                                type="button"
                                className="btn btn-icon btn-info  box-shadow-1 mr-1 mb-1"
                                title=""
                                data-original-title="إضافة"
                                onClick={e => this.viewModal(e, "addModal")}
                            >
                                <i className="la la-add"></i> إضافة كود جديد
                            </button>
                        {renderAddModal}

                        <section className="card">
                            <div className="card-content">
                                <div className="card-body">
                                    <div className="card-text">
                                        <div className="table-striped">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th
                                                            scope="col"
                                                            className="table-bordered "
                                                        >
                                                            #
                                                        </th>
                                                        <th scope="col">
                                                            الكود
                                                        </th>
                                                        <th scope="col">
                                                            القيمة
                                                        </th>
                                                        <th scope="col">
                                                            الحالة
                                                        </th>
                                                        <th
                                                            scope="col"
                                                            className="text-center"
                                                        >
                                                            الخيارات
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>{renderItems}</tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }
}
export default Codes;
