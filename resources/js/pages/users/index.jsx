import React, { Component, useContext } from 'react';
//import {AuthContext} from '../../contexts/AuthContext';
import { Link } from 'react-router-dom';
import { AxiosCall, ImageBasePath }  from './../../helpers/Shared';
import Breadcrumb from './../../components/Breadcrumb';

class Users extends Component{
    
    state = {
        userName: '',
        userStatus: '',
        admin: "",
        items: [],
        perPage: 10,
        page: 0,
        last_page: "",
        first_page_url: "",
        last_page_url: "",
        next_page_url: "",
        prev_page_url: "",
    }

    getData = (key = null, status = null)  => {
        let endPoint = `get-users`;
        endPoint += key === null ? '?key=' : `?key=${key}`;
        endPoint += status === null ? '&status=' : `&status=${status}`;

        let response = AxiosCall(endPoint,'GET');
        response.then( (result) => result.data)
        .then( (data) => data.data)
        .then( (data) => {
            this.setState({
                admin: data.admin,
                items:  data.perPage > 0 ? data.items.data : data.items,
                perPage: data.perPage, 
                last_page: data.perPage > 0 ? data.last_page : '', 
                next_page_url: data.perPage > 0 ? data.next_page_url : '', 
                prev_page_url: data.perPage > 0 ? data.prev_page_url : '', 
                first_page_url: data.perPage > 0 ? data.first_page_url : '', 
                last_page_url: data.perPage > 0 ? data.last_page_url : '', 
                page: data.page, 
            });
        });
    }

    componentDidMount() {
        this.getData();
    }

    viewModal = (viewID) => {
        let modal = window.$("#"+viewID).modal("show");
    }

    deleteModal = async (id, modalID) =>  {
        window.$("#"+modalID).modal("hide");
        let endPoint = "delete-user/"+id;
        await AxiosCall(endPoint,'DELETE');
        this.setState({
            items: this.state.items.filter( (item) => item.id !== id )
        });
        
    }

    handleSearchSubmit = (e) => {
        e.preventDefault();
        this.getData(this.state.userName, this.state.userStatus)
    }
    handleSearchChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
        let vm = this;
        setTimeout(function(){
            vm.getData(vm.state.userName, vm.state.userStatus)
        }, 100)
        
        //console.log('handleSearchChange', this.state);
    }

    render() {
        const renderItems = this.state.items.map( (item, index) => {
            let status = item.status ? "badge badge-success" : "badge badge-danger";
            let thumb = item.status ? "la la-thumbs-o-up" : "la la-thumbs-o-down";
            let isActive = item.status ? "مفعل" : "غير مفعل";
            let viewID = "viewModal_"+item.id;
            let deleteID = "deleteModal_"+item.id;
            

            const renderViewModal = (
                <div className="modal fade text-left" id={viewID} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <h4 className="text-center mb-2">بيانات العضويه</h4>
                                <table className="table">
                                    <tbody>
                                        <tr>
                                            <td>الاسم</td>
                                            <td>
                                                <span className="avatar avatar-sm rounded-circle mr-1">
                                                    <img src={ImageBasePath+'/users/'+item.image} alt="" />
                                                </span>
                                                {item.name}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> البريد الالكترونى </td>
                                            <td>{item.email}</td>
                                        </tr>
                                        <tr>
                                            <td> رقم الجوال </td>
                                            <td>{item.phone}</td>
                                        </tr>
                                        <tr>
                                            <td> الحالة </td>
                                            <td><span className={status} data-toggle="tooltip" data-placement="top" title="" data-original-title={isActive}><i className={thumb}></i></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="modal-footer bg-flight">
                                <button type="button" className="btn grey btn-outline-light" data-dismiss="modal"><i className="la la-times-circle pr-1"></i>اغلاق</button>
                            </div>
                        </div>
                    </div>
                </div>
            );
            
            const renderDeleteModal = (
                <div className="modal fade text-left" id={deleteID} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <h5 className="text-center mb-2"> هل تريد اتمام الحذف؟</h5>
                            </div>
                            <div className="modal-footer bg-flight">
                                <button type="button" className="btn grey btn-outline-danger" onClick={ () => this.deleteModal(item.id, deleteID) }><i className="la la-times-circle pr-1"></i>حذف</button>
                                <button type="button" className="btn grey btn-outline-light" data-dismiss="modal"><i className="la la-times-circle pr-1"></i>اغلاق</button>
                            </div>
                        </div>
                    </div>
                </div>
            );

            return (
                <tr key={item.id}>
                    
                    <th scope="row" className="table-bordered ">{index + 1}</th>
                    <td>
                        <span className="avatar avatar-sm rounded-circle mr-1">
                            <img src={ImageBasePath+'/users/'+item.image} alt="" />
                        </span>
                        {item.name}
                    </td>
                    <td> {item.email} </td>
                    <td> {item.phone} </td>
                    <td><div className={status} data-toggle="tooltip" data-placement="top" title="" data-original-title={isActive}><i className={thumb}></i></div>
                    </td>
                    <td>
                        <div className="float-md-right ">
                            <button type="button" className="btn btn-icon btn-success  box-shadow-1 mr-1 mb-1" title="" data-original-title="مشاهده" onClick={ () => this.viewModal(viewID)}><i className="la la-eye"></i></button>
                            {renderViewModal}
                            <Link 
                                type="button" 
                                className="btn btn-icon btn-warning  box-shadow-1 mr-1 mb-1" 
                                data-toggle="tooltip" 
                                data-placement="top" 
                                title="" 
                                data-original-title="تعديل" 
                                to={{
                                    pathname: '/edit-user/'+item.id,
                                    state: {
                                        edit: true,
                                        item: item
                                    }
                                }}
                                >  
                                <i className="la la-edit"></i>
                            </Link>
                            <button type="button" className="btn btn-icon  btn-danger  box-shadow-1 mr-1 mb-1" title="" data-original-title="حذف" onClick={ () => this.viewModal(deleteID)}><i className="la la-trash"></i></button>
                            {renderDeleteModal}
                            
                        </div>
                    </td>
                </tr>
            );
        });
        
        return (
            <div className="app-content content">
                <div className="content-wrapper">
                    <div className="content-body">

                        <Breadcrumb routeName="الأعضاء" linkTo="/" />

                        <div className="card">
                            <div className="card-content">
                                <section className="card  pt-0 pb-0">
                                    <div className="card-content">
                                        <div className="card-header">
                                            <div className="float-left">
                                                <h4 className="mb-0"><strong>البحث</strong></h4>
                                            </div>
                                            <div className="float-right">
                                                
                                            </div>
                                        </div>
                                        <div className="card-body pt-1 pb-0">
                                        <div className="card-text">
                                            <form onSubmit={this.handleSearchSubmit}>
                                                <div className="form-body">
                                                    <div className="row">
                                                        <div className="col-md-4 pr-0">
                                                            <div className="form-group">
                                                            <input onChange={this.handleSearchChange} type="text" className="form-control" placeholder='بحث بالاسم، رقم الجوال أو البريد الالكترونى' name='userName' value={this.state.userName} />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-2 pr-0">
                                                            <div className="form-group">
                                                            <select onChange={this.handleSearchChange} className="form-control" value={this.state.userStatus} name='userStatus' id="">
                                                                <option value="">الكل</option>
                                                                <option value={0}>غير مفعل</option>
                                                                <option value={1}>مفعل</option>
                                                            </select>
                                                            </div>
                                                        </div>

                                                        <div className="col-md-1">
                                                            <button type="submit" className="btn btn-icon btn-light float-right box-shadow-1 mt-0 mb-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="بحث"><i className="la la-filter"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>

                        <section className="card">
                            <div className="card-content">
                                <div className="card-body">
                                    <div className="card-text">
                                        <div className="table-striped">
                                            <table className="table">
                                                <thead >
                                                    <tr>
                                                        <th scope="col" className="table-bordered ">#</th>
                                                        <th scope="col">الاسم</th>
                                                        <th scope="col">البريد الالكترونى</th>
                                                        <th scope="col">رقم الجوال</th>
                                                        <th scope="col">الحالة</th>
                                                        <th scope="col"className="text-center">خيارات</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {renderItems}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }
    
}
export default Users;