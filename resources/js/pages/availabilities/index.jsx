import React, { Component, useContext } from "react";
import {
    AxiosCall,
    ImageBasePath,
    SendPostRequest
} from "../../helpers/Shared";
import Breadcrumb from "../../components/Breadcrumb";

class Categories extends Component {
    state = {
        time_from: "",
        time_to: "",
        isLoading: true,
        admin: "",
        items: null,
        perPage: 10,
        page: 0,
        last_page: "",
        first_page_url: "",
        last_page_url: "",
        next_page_url: "",
        prev_page_url: ""
    };

    getData = (filters = null) => {
        let endPoint = "get-availabilities";

        let response = AxiosCall(endPoint, "GET");
        response
            .then(result => result.data)
            .then(data => data.data)
            .then(data => {
                //console.log('Get Data 2', data)
                this.setState({
                    isLoading: false,
                    admin: data.admin,
                    perPage: data.perPage,
                    items: data.perPage > 0 ? data.items.data : data.items,
                    last_page: data.perPage > 0 ? data.items.last_page : "",
                    next_page_url:
                        data.perPage > 0 ? data.items.next_page_url : "",
                    prev_page_url:
                        data.perPage > 0 ? data.items.prev_page_url : "",
                    first_page_url:
                        data.perPage > 0 ? data.items.first_page_url : "",
                    last_page_url:
                        data.perPage > 0 ? data.items.last_page_url : "",
                    page: data.page
                });
            });
    };

    componentWillMount() {
        this.getData();
    }

    viewModal = viewID => {
        window.$("#" + viewID).modal("show");
    };

    deleteModal = async (id, modalID) => {
        window.$("#" + modalID).modal("hide");
        let endPoint = "delete-availability/" + id;
        await AxiosCall(endPoint, "DELETE");
        this.setState({
            items: this.state.items.filter(item => item.id !== id)
        });
    };

    handleEditChange = toUpdateItem => e => {
        if (e.target.name == "time_from") {
            toUpdateItem.time_from = e.target.value;
        } else if (e.target.name == "time_to") {
            toUpdateItem.time_to = e.target.value;
        }
        //console.log(toUpdateItem);
        if (this.state.items.length > 0) {
            this.setState(state => {
                const items = state.items.map(item => {
                    if (item.id === toUpdateItem.id) {
                        return toUpdateItem;
                    } else {
                        return item;
                    }
                });
                return {
                    items
                };
            });
        }
    };
    handleSubmitEdit = item => e => {
        e.preventDefault();
        let endPoint = "edit-availability/" + item.id;
        let data = [
            { key: "_method", value: "PUT" },
            { key: "time_from", value: item.time_from },
            { key: "time_to", value: item.time_to }
        ];
        let res = SendPostRequest(endPoint, data);
        res.then(
            response => {
                alert(response.data.message);
                console.log(response);
                window.$("#editModal_" + item.id).modal("hide");
            },
            error => {
                alert(error.response.message);
                //console.log(error.response);
            }
        );
    };

    handleAddChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };
    handleSubmitAdd = e => {
        e.preventDefault();
        let endPoint = "add-availability";
        let data = [
            { key: "time_from", value: this.state.time_from },
            { key: "time_to", value: this.state.time_to }
        ];

        let res = SendPostRequest(endPoint, data);
        res.then(
            response => {
                let newItems = [...this.state.items];
                let itemToAdd = response.data.data;

                newItems.unshift(itemToAdd);
                this.setState({
                    items: newItems,
                    time_from: "",
                    time_to: ""
                });
                window.$("#addModal").modal("hide");
            },
            error => {
                //alert(error.response.data.message);
                alert(error.response.statusText);
                console.log(error.response);
            }
        );
    };

    render() {
        let renderItems = (
            <tr>
                <th scope="col" className="table-bordered ">
                    ---
                </th>
                <th scope="col">---</th>
                <th scope="col">---</th>
                <th scope="col" className="text-center">
                    ---
                </th>
            </tr>
        );
        if (this.state.items !== null) {
            renderItems = this.state.items.map((item, index) => {
                let viewID = "viewModal_" + item.id;
                let editID = "editModal_" + item.id;
                let deleteID = "deleteModal_" + item.id;

                const renderEditModal = (
                    <div
                        className="modal fade text-left"
                        id={editID}
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel1"
                        aria-hidden="true"
                    >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <h4 className="text-center mb-2">
                                        كافة البيانات
                                    </h4>
                                    <form
                                        onSubmit={this.handleSubmitEdit(item)}
                                    >
                                        <table className="table">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label htmlFor="">
                                                            الوقت من
                                                        </label>
                                                        <input
                                                            type="text"
                                                            name="time_from"
                                                            className="form-control"
                                                            onChange={this.handleEditChange(
                                                                item
                                                            )}
                                                            value={
                                                                item.time_from
                                                            }
                                                        />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label htmlFor="">
                                                            الوقت إلى
                                                        </label>
                                                        <input
                                                            type="text"
                                                            name="time_to"
                                                            className="form-control"
                                                            onChange={this.handleEditChange(
                                                                item
                                                            )}
                                                            value={item.time_to}
                                                        />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input
                                                            type="submit"
                                                            className="btn btn-success"
                                                            value="حفظ"
                                                        />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <div className="modal-footer bg-flight">
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-light"
                                        data-dismiss="modal"
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        اغلاق
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                const renderDeleteModal = (
                    <div
                        className="modal fade text-left"
                        id={deleteID}
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="myModalLabel1"
                        aria-hidden="true"
                    >
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <h5 className="text-center mb-2">
                                        {" "}
                                        هل تريد اتمام الحذف؟
                                    </h5>
                                </div>
                                <div className="modal-footer bg-flight">
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-danger"
                                        onClick={() =>
                                            this.deleteModal(item.id, deleteID)
                                        }
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        حذف
                                    </button>
                                    <button
                                        type="button"
                                        className="btn grey btn-outline-light"
                                        data-dismiss="modal"
                                    >
                                        <i className="la la-times-circle pr-1"></i>
                                        اغلاق
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                );

                return (
                    <tr key={item.id}>
                        <th scope="row" className="table-bordered ">
                            {index + 1}
                        </th>
                        <td>{item.time_from}</td>
                        <td>{item.time_to}</td>
                        <td>
                            <div className="float-md-right ">
                                <button
                                    type="button"
                                    className="btn btn-icon btn-warning  box-shadow-1 mr-1 mb-1"
                                    title=""
                                    data-original-title="تعديل"
                                    onClick={() => this.viewModal(editID)}
                                >
                                    <i className="la la-edit"></i>
                                </button>
                                {renderEditModal}

                                <button
                                    type="button"
                                    className="btn btn-icon  btn-danger  box-shadow-1 mr-1 mb-1"
                                    title=""
                                    data-original-title="حذف"
                                    onClick={() => this.viewModal(deleteID)}
                                >
                                    <i className="la la-trash"></i>
                                </button>
                                {renderDeleteModal}
                            </div>
                        </td>
                    </tr>
                );
            });
        }

        const renderAddModal = (
            <div
                className="modal fade text-left"
                id="addModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="myModalLabel1"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <h4 className="text-center mb-2">كافة البيانات</h4>
                            <form onSubmit={this.handleSubmitAdd}>
                                <table className="table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label htmlFor="">
                                                    الوقت من
                                                </label>
                                                <input
                                                    type="text"
                                                    name="time_from"
                                                    className="form-control"
                                                    onChange={
                                                        this.handleAddChange
                                                    }
                                                    value={this.state.time_from}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label htmlFor="">
                                                    الوقت إلى
                                                </label>
                                                <input
                                                    type="text"
                                                    name="time_to"
                                                    className="form-control"
                                                    onChange={
                                                        this.handleAddChange
                                                    }
                                                    value={this.state.time_to}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input
                                                    type="submit"
                                                    className="btn btn-success"
                                                    value="حفظ"
                                                />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <div className="modal-footer bg-flight">
                            <button
                                type="button"
                                className="btn grey btn-outline-light"
                                data-dismiss="modal"
                            >
                                <i className="la la-times-circle pr-1"></i>اغلاق
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
        return (
            <div className="app-content content">
                <div className="content-wrapper">
                    <div className="content-body">
                        <Breadcrumb routeName="المواعيد المتاحة" linkTo="/" />
                        <button
                            type="button"
                            className="btn btn-icon btn-info  box-shadow-1 mr-1 mb-1"
                            title=""
                            data-original-title="إضافة"
                            onClick={() => this.viewModal("addModal")}
                        >
                            <i className="la la-add"></i> إضافة موعد جديد
                        </button>
                        {renderAddModal}

                        <section className="card">
                            <div className="card-content">
                                <div className="card-body">
                                    <div className="card-text">
                                        <div className="table-striped">
                                            <table className="table">
                                                <thead>
                                                    <tr>
                                                        <th
                                                            scope="col"
                                                            className="table-bordered "
                                                        >
                                                            #
                                                        </th>
                                                        <th scope="col">
                                                            الوقت من
                                                        </th>
                                                        <th scope="col">
                                                            الوقت إلى
                                                        </th>
                                                        <th
                                                            scope="col"
                                                            className="text-center"
                                                        >
                                                            الخيارات
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>{renderItems}</tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }
}
export default Categories;
