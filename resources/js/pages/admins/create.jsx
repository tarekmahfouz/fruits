import React, { Component } from 'react';

import { AxiosCall } from '../../helpers/Shared';

class CreateAdmin extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        status: 1,
        image: null,
        password: '',
        password_confirmation: '',
    }

    handleChange = (e) => {
        if(e.target.name !== 'image') {
            this.setState({
                [e.target.name] : e.target.value
            });
        } else {
            this.setState({
                image: e.target.files[0]
            });
        }
        console.log(e.target.value);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let endPoint = "add-admin";

        let formData = new FormData();
        
        formData.append('name', this.state.name);
        formData.append('phone', this.state.phone);
        formData.append('email', this.state.email);
        formData.append('status', this.state.status);
        formData.append('password', this.state.password);
        formData.append('password_confirmation', this.state.password_confirmation);
        if(this.state.image != null) {
            formData.append('image', this.state.image);
        }
        let headers = { 'content-type': 'multipart/form-data' };
        let res = AxiosCall(endPoint,'POST', formData, headers);
        
        res.then(
            (response) => { 
                alert(response.data.message);
                this.setState({
                    name: '',
                    phone: '',
                    email: '',
                    status: 1,
                    image: null,
                    password: '',
                    password_confirmation: '',
                });
            },
            (error) => { 
                alert(error.response.data.message);
                console.log(error.response.data.message) 
            }
        );
    }

    render() {
        let {name, phone, email, status, password, password_confirmation} = this.state;

        return (
            <div className="app-content content">
                <div className="content-wrapper">
                    <div className="content-body">
                    
                    <section className="card">
                        <div className="card-content">
                        <div className="card-body">
                            <div className="card-text">
                                
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-body">
                                <h4 className="form-section"><i className="la la-caret-square-o-left"></i>بيانات العضوية</h4>
                                
                                <div className="row">
                                    <div className="col-md-6">
                                    <div className="form-group">
                                        <label >الاسم</label>
                                        <input type="text" className="form-control success" placeholder="name" name="name" value={name}  onChange={this.handleChange}/>
                                    </div>
                                    </div>
                                    <div className="col-md-6">
                                    <div className="row">
                                        <div className="col-md-6">
                                        <div className="form-group">
                                            <label >كلمة المرور</label>
                                            <input type="password"  className="form-control" placeholder="كلمة المرور" name="password" value={password}  onChange={this.handleChange}/>
                                        </div>
                                        </div>

                                        <div className="col-md-6">
                                        <div className="form-group">
                                            <label >تأكيد كلمة المرور</label>
                                            <input type="password"  className="form-control" placeholder="تأكيد كلمة المرور" name="password_confirmation" value={password_confirmation}  onChange={this.handleChange}/>
                                        </div>
                                        </div>
                                    </div>
                                    </div>

                                </div>
                                
                                <div className="row">
                                    <div className="col-md-6">
                                    <div className="form-group">
                                        <label >البريد الالكترونى</label>
                                        <input type="text" className="form-control success" placeholder="البريد الالكترونى" name="email" value={email}  onChange={this.handleChange}/>
                                    </div>
                                    </div>
                                    <div className="col-md-6">
                                    <div className="form-group">
                                        <label >رقم الجوال</label>
                                        <input type="text" className="form-control success" placeholder="رقم الجوال" name="phone" value={phone} onChange={this.handleChange}/>
                                    </div>
                                    </div>
                                </div>
                                
                                <div className="row">
                                    <div className="col-md-6">
                                    <div className="form-group">
                                        <label >الحالة</label>
                                        <select defaultValue={status} name="status" className="select2 form-control" onChange={this.handleChange}>
                                        <option value={1}>مفعل</option>
                                        <option value={0}>غير مفعل</option>

                                        </select>
                                    </div>
                                    </div>
                                    <div className="col-md-6  ">

                                    <div className="form-group">
                                        <label >اختر صورة</label>
                                        <input name="image" type="file" className="form-control"  onChange={this.handleChange}/>
                                    </div>

                                    </div>
                                </div>
                                </div>

                                <div className="form-actions right  ">
                                <button type="submit" className="btn btn-success">
                                    <i className="la la-check-square-o pr-1"></i>حفظ
                                </button>
                                </div>

                            </form>

                            </div>
                        </div>
                        </div>
                    </section>
                    </div>
                </div>
            </div>

        );
    }
}
 
export default CreateAdmin;