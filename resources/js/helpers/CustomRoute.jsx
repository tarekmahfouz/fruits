import React, { Component, useContext } from 'react';
import {BrowserRouter , Route, Redirect}  from 'react-router-dom';

const CustomRoute = (props) => {
    const {component: Component, ...rest} = props;
    console.log(props);

    return (
        <Route {...rest} component={() => <Component {...props} />} />
        /* <Route {...rest} render={ (props) => (
            <Component {...props}/>
        )} /> */
    ); 
}
 
export default CustomRoute;