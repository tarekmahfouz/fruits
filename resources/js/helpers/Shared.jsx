import Axios from "axios";

//export const BaseURL = "http://localhost/fruits/public/admin-panel";
export const BaseURL = "https://www.fruits-app.com/fruits-system/public/admin-panel";
export const ImageBasePath = "/fruits-system/public/images";

export function AxiosCall(endpoint, method = "get", params = {}, headers = {}) {
  let url = BaseURL + "/" + endpoint;
  let axiosResponse = Axios({
    method,
    url,
    data: params,
    headers,
  });
  return axiosResponse;
}

export function SendPostRequest(endPoint, data, headers = {}) {
  let formData = new FormData();
  data.forEach((item) => {
    formData.append(item.key, item.value);
  });
  let res = AxiosCall(endPoint, "POST", formData, headers);
  return res;
}
