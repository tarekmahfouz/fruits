import React, { Component } from "react";

import { Link, withRouter } from "react-router-dom";
import { AxiosCall } from "./../helpers/Shared";

class Sidebar extends Component {
  state = {
    ordersCount: 0,
  };
  getData = () => {
    let endPoint = "get-orders-count";
    let response = AxiosCall(endPoint, "GET");
    response
      .then((result) => result.data)
      .then((data) => data.data)
      .then((data) => {
        this.setState({
          ordersCount: data.ordersCount,
        });
      });
  };
  componentWillMount() {
    this.getData();
  }

  render() {
    return (
      <div
        className="main-menu menu-fixed menu-light menu-accordion menu-shadow"
        data-scroll-to-active="true"
      >
        <div className="main-menu-content">
          <ul
            className="navigation navigation-main"
            data-menu="menu-navigation"
          >
            <li className="nav-item">
              <Link to="/">
                <i className="la la-cog"></i>
                <span className="menu-title"> الخصائص العامة </span>
              </Link>
            </li>

            <li className=" nav-item">
              <a href="#">
                <i className="la la-black-tie"></i>
                <span className="menu-title"> الإدارة العامة </span>
              </a>

              <ul className="menu-content">
                <li>
                  <Link className="menu-item" to="/admins">
                    {" "}
                    المديرين{" "}
                  </Link>
                </li>
                <li>
                  <Link
                    className="menu-item"
                    to={{
                      pathname: "/create-admin",
                      state: {
                        edit: false,
                      },
                    }}
                  >
                    {" "}
                    إضافة مدير{" "}
                  </Link>
                </li>
              </ul>
            </li>

            <li className=" nav-item">
              <a href="#">
                <i className="la la-users"></i>
                <span className="menu-title"> العملاء </span>
              </a>

              <ul className="menu-content">
                <li>
                  <Link className="menu-item" to="/users">
                    {" "}
                    العملاء{" "}
                  </Link>
                </li>
                <li>
                  <Link className="menu-item" to="/create-user">
                    {" "}
                    إضافة عميل جديد{" "}
                  </Link>
                </li>
              </ul>
            </li>

            <li className=" nav-item {{(request()->route()->getName()=='admin.get.availabilities')?'active':''}}">
              <Link className="menu-item" to="/availabilities">
                <i className="la la-clock-o"></i>
                <span className="menu-title"> المواعيد المتاحة للتوصيل </span>
              </Link>
            </li>

            <li className=" nav-item {{(request()->route()->getName()=='admin.get.weights')?'active':''}}">
              <Link className="menu-item" to="/weights">
                <i className="la la-balance-scale"></i>
                <span className="menu-title">{" الأوزان "}</span>
              </Link>
            </li>

            <li className=" nav-item {{(request()->route()->getName()=='admin.codes')?'active':''}}">
              <Link className="menu-item" to="/codes">
                <i className="la la-cog"></i>
                <span className="menu-title"> أكواد الخصم </span>
              </Link>
            </li>

            <li className=" nav-item {{(request()->route()->getName()=='admin.properties')?'active':''}}">
              <Link className="menu-item" to="/categories">
                <i className="la la-cog"></i>
                <span className="menu-title"> التصنيفات الرئيسية </span>
              </Link>
            </li>

            <li className=" nav-item">
              <a href="#">
                <i className="la la-pinterest-p"></i>
                <span className="menu-title"> المنتجات </span>
              </a>

              <ul className="menu-content">
                <li>
                  <Link className="menu-item" to="/products">
                    {" "}
                    المنتجات{" "}
                  </Link>
                </li>
                <li>
                  <Link
                    className="menu-item"
                    to={{
                      pathname: "/create-product",
                      state: {
                        edit: false,
                      },
                    }}
                  >
                    {" "}
                    إضافة منتج{" "}
                  </Link>
                </li>
              </ul>
            </li>

            <li className="">
              <Link className="menu-item" to="/orders">
                <i className="la la-paypal"></i>
                <span className="menu-title float-left">طلبات العملاء </span>
                <i className="badge badge-danger float-right">
                  {this.state.ordersCount}
                </i>
              </Link>
            </li>

            <li className="">
              <Link className="menu-item" to="/inbox">
                <i className="la la-envelope"></i>
                <span className="menu-title"> الرسائل الواردة </span>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default withRouter(Sidebar);
