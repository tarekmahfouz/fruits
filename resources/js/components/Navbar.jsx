import React, { Component } from "react";

import { AxiosCall, ImageBasePath } from "./../helpers/Shared";
import { Link } from "react-router-dom";

class Navbar extends Component {
    state = {
        adminID: 0,
        adminName: "",
        adminImage: ""
    };

    getData = () => {
        let endPoint = "me";
        let response = AxiosCall(endPoint, "GET");
        response
            .then(result => result.data)
            .then(data => data.data)
            .then(data => {
                this.setState({
                    adminID: data.id,
                    adminName: data.name,
                    adminImage: data.image
                });
            });
    };

    componentWillMount() {
        this.getData();
    }
    render() {
        return (
            <nav className="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
                <div className="navbar-wrapper">
                    <div className="navbar-header">
                        <ul className="nav navbar-nav flex-row position-relative">
                            <li className="nav-item mobile-menu d-md-none mr-auto">
                                <a
                                    className="nav-link nav-menu-main menu-toggle hidden-xs"
                                    href="#"
                                >
                                    <i className="ft-menu font-large-1"></i>
                                </a>
                            </li>
                            <li className="nav-item mr-auto">
                                <a className="navbar-brand" href="#">
                                    <img
                                        className="brand-logo"
                                        style={{
                                            height: "30px",
                                            width: "30px",
                                            fontSize: "12px"
                                        }}
                                        src={ImageBasePath + "/provider.png"}
                                        alt="تمكين"
                                    />
                                    <h3 className="brand-text">
                                        {" "}
                                        الفواكة والخضار{" "}
                                    </h3>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="navbar-container content">
                        <div
                            className="collapse navbar-collapse"
                            id="navbar-mobile"
                        >
                            <ul className="nav navbar-nav mr-auto float-left">
                                <li className="nav-item d-none d-md-block">
                                    <a
                                        className="nav-link nav-link-expand"
                                        href="#"
                                    >
                                        <i className="ficon ft-maximize"></i>
                                    </a>
                                </li>
                            </ul>

                            <ul className="nav navbar-nav float-right">
                                <li className="dropdown dropdown-user nav-item">
                                    <a
                                        className="dropdown-toggle nav-link dropdown-user-link"
                                        href="/"
                                        data-toggle="dropdown"
                                    >
                                        <span className="user-name mr-1">
                                            {" "}
                                            {this.state.adminName}
                                        </span>
                                        <span className="avatar avatar-online">
                                            <img
                                                src={
                                                    ImageBasePath +
                                                    "/users/" +
                                                    this.state.adminImage
                                                }
                                                alt=""
                                            />
                                            <i></i>
                                        </span>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <Link
                                            className="dropdown-item"
                                            to={
                                                "/edit-admin/" +
                                                this.state.adminID
                                            }
                                        >
                                            <i className="ft-edit"></i>تعديل
                                            بياناتي
                                        </Link>
                                        <div className="dropdown-divider"></div>
                                        <a
                                            className="dropdown-item"
                                            href="admin-panel/logout"
                                        >
                                            <i className="ft-power"></i>تسجيل
                                            خروج
                                        </a>
                                    </div>
                                </li>

                                <li className="dropdown dropdown-language nav-item">
                                    <a
                                        className="dropdown-toggle nav-link"
                                        id="dropdown-flag"
                                        href="#"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                    >
                                        <i className="flag-icon flag-icon-sa"></i>
                                        <span className="selected-language"></span>
                                    </a>
                                    <div
                                        className="dropdown-menu"
                                        aria-labelledby="dropdown-flag"
                                    >
                                        <a className="dropdown-item" href="#">
                                            <i className="flag-icon flag-icon-sa"></i>{" "}
                                            عربي
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Navbar;
