import React from 'react';
import {v1 as uuidv1} from 'uuid';

const Search = (props) => {
    return (
        <div className="card">
            <div className="card-content">
                <section className="card  pt-0 pb-0">
                    <div className="card-content">
                        <div className="card-body pt-1 pb-0">
                        <div className="card-text">
                            <form onSubmit={props.handleSearchSubmit}>
                            <div className="form-body">
                                <div className="row">
                                {props.inputs.map(input => {
                                    return (
                                        <div key={uuidv1()} className="col-md-3 pr-0">
                                            <div className="form-group">
                                            <input onChange={props.handleSearchChange} type="text" className="form-control " placeholder={input.placeholder} name={input.name} value={input.value} />
                                            </div>
                                        </div>
                                    )
                                })}
                                {props.selects.map(select => {
                                    return (
                                        <div key={uuidv1()} className="col-md-2 pr-0">
                                            <div className="form-group">
                                            <select onChange={props.handleSearchChange} value={select.defaultValue} name={select.name} id="">
                                            {select.options.map(option => {
                                                return (
                                                    <option value={option}>{option}</option>
                                                );
                                            })}
                                            </select>
                                            </div>
                                        </div>
                                    )
                                })}
                                <div className="col-md-1">
                                    <button type="submit" className="btn btn-icon btn-light float-right box-shadow-1 mt-0 mb-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="بحث"><i className="la la-filter"></i>
                                    </button>
                                </div>
                                </div>
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}
 
export default Search;