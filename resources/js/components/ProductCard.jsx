import React, { Component } from 'react'

import { Link } from 'react-router-dom';
import { ImageBasePath } from './../helpers/Shared';

class ProductCard extends Component {
    state = {  }
    
    viewModal = (modalID) => {
        window.$("#"+modalID).modal("show");
    }

    render() { 
        let { productID, name, image, price, netPrice, discount, category, cName} = this.props;
        let modalID = 'delete_model_'+productID;
        let deleteModal = (
            <div className="modal fade text-left" id={modalID} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <h5 className="text-center mb-2"> هل تريد اتمام الحذف؟</h5>
                        </div>
                        <div className="modal-footer bg-flight">
                            <button type="button" className="btn grey btn-outline-danger" onClick={ () => this.props.deleteModal(productID, modalID) }><i className="la la-times-circle pr-1"></i>حذف</button>
                            <button type="button" className="btn grey btn-outline-light" data-dismiss="modal"><i className="la la-times-circle pr-1"></i>اغلاق</button>
                        </div>
                    </div>
                </div>
            </div>
        );
        return (
            <div className={cName}>
                <div className="card pull-up">
                    <div className="card-content">
                        <div className="card-body">
                            <Link to={"/edit-product/"+productID}>
                                <div className="product-img d-flex align-items-center">
                                    <div className="badge badge-success round">-{discount}%</div>
                                    <img className="img-fluid mb-1" src={ImageBasePath+'/products/'+image} alt="Card image cap" />
                                </div>
                                <h4 className="product-title">{name}</h4>
                                <div className="price-reviews">
                                    <span className="price-box float-left">
                                        <span className="price">{netPrice}</span> &nbsp;
                                        <span className="old-price">{price}</span>
                                    </span>
                                    <span className="price-box float-right">
                                        <span style={{color: '#aaa'}} className="price">{category}</span>
                                    </span>
                                    <span className="ratings float-right"></span>
                                </div>
                            </Link>
                            <div className="product-action d-flex justify-content-around">
                                
                                <Link to={"/edit-product/"+productID} className="btn btn-icon btn-warning  box-shadow-1 mr-1 mb-1" title="" data-original-title="تعديل">
                                    <i className="la la-edit"></i>
                                </Link>
                                
                                <button type="button" className="btn btn-icon  btn-danger  box-shadow-1 mr-1 mb-1" title="" data-original-title="حذف">
                                    <i className="la la-trash" onClick={ () => this.viewModal(modalID)}></i>
                                </button>
                                
                            </div>
                        </div>
                    </div>
                </div>
                {deleteModal}
            </div>
        );
    }
}
 
export default ProductCard;