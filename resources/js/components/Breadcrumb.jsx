import React from 'react'
import { Link } from 'react-router-dom';

const Breadcrumb = (props) => {
    return (
        <div className="card">
            <div className="card-content">
                <div className="content-header-left col-md-6 col-12 mb-2 breadcrumb-new ">
                    <h3 className="content-header-title mb-0 d-inline-block">{props.routeName}</h3>
                    <div className="row breadcrumbs-top d-inline-block">
                    <div className="breadcrumb-wrapper col-12">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <Link to={props.linkTo}>الخصائص العامة</Link>
                            </li>
                            <li className="breadcrumb-item active">{props.routeName}</li>
                        </ol>
                    </div>
                    </div>
                </div>
                {/* <div className="card-body">
                    <div className="d-flex justify-content-around lh-condensed">
                        
                    </div>
                </div> */}
            </div>
        </div>
    );
}
 
export default Breadcrumb;