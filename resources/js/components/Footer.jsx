import React, { Component } from 'react'
import { ImageBasePath }  from './../helpers/Shared';

class Footer extends Component {
    state = {  }
    render() { 
        return (
            <footer className="footer footer-static footer-light navbar-border navbar-shadow" style={{}}>
                <p className="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
                    <span className="float-md-left d-block d-md-inline-block">
                        <a href="http://www.tamkeen-apps.com" target="_blank">
                            <img style={{height: '30px', width: '30px'}} src={ImageBasePath+'/provider.png'} alt=" شركه تمكين لتقنيه المعلومات" /></a> إحدى برامج شركة <a href="http://www.tamkeen-apps.com" target="_blank" >تمكين</a> لتقنية المعلومات
                    </span>
                    <span className="float-md-right d-block d-md-inline-blockd-none d-lg-block">
                        All rights reserved &nbsp;  <a href="http://www.tamkeen-apps.com" target="_blank">Tamkeen Co.</a>&nbsp; For Information Technology
                    </span>
                </p>
            </footer>
        );
    }
}
 
export default Footer;