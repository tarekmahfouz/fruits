import React, { Component } from "react";
import { BrowserRouter, HashRouter, Route, Switch } from "react-router-dom";
// import { createBrowserHistory } from 'history'

//import { AuthContext } from './contexts/AuthContext';

import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import Footer from "./components/Footer";

import Admins from "./pages/admins/index";
import EditAdmin from "./pages/admins/edit";
import CreateAdmin from "./pages/admins/create";

import Users from "./pages/users/index";
import CreateUser from "./pages/users/create";
import EditUser from "./pages/users/edit";

import Categories from "./pages/categories/index";

import Config from "./pages/system/config";

import Availabilities from "./pages/availabilities/index";

import Weights from "./pages/weights/index";

import Products from "./pages/products/index";
import CreateProduct from "./pages/products/create";
import EditProduct from "./pages/products/edit";

import Codes from "./pages/codes/index";

import Inbox from "./pages/inbox/index";

import Orders from "./pages/orders/index";
import Order from "./pages/orders/show";

export default class Main extends Component {
  state = {};

  render() {
    return (
      <HashRouter>
        <Navbar />
        <Sidebar />
        <Switch>
          <Route exact path="/" component={Config} />
          <Route path="/config" component={Config} />

          <Route path="/admins" component={Admins} />
          <Route path="/create-admin" component={CreateAdmin} />
          <Route path="/edit-admin/:id" component={EditAdmin} />

          <Route path="/users" component={Users} />
          <Route path="/create-user" component={CreateUser} />
          <Route path="/edit-user/:id" component={EditUser} />

          <Route path="/categories" component={Categories} />

          <Route path="/availabilities" component={Availabilities} />

          <Route path="/codes" component={Codes} />

          <Route path="/weights" component={Weights} />

          <Route path="/products" component={Products} />
          <Route path="/edit-product/:id" component={EditProduct} />
          <Route path="/create-product" component={CreateProduct} />

          <Route path="/orders" component={Orders} />
          <Route path="/order/:id" component={Order} />

          <Route path="/inbox" component={Inbox} />
        </Switch>
        <Footer />
      </HashRouter>
    );
  }
}
