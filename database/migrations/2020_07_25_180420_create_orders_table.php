<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('cart_id')->unsigned();
            $table->bigInteger('payment_way_id')->unsigned()->nullable();
            $table->string('time_from');
            $table->string('time_to');
            $table->string('lat');
            $table->string('lon');
            $table->string('address')->nullable();
            $table->text('notes')->nullable();
            $table->enum('status', ['preview', 'onway', 'confirmed', 'rejected', 'delivered'])->default('preview');
            $table->float('shipping_value')->default(0);
            $table->string('promo_code')->nullable();
            $table->float('promo_code_value')->default(0)->nullable();
            $table->double('total_cost')->default(0);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('cart_id')->references('id')->on('carts');
            $table->foreign('payment_way_id')->references('id')->on('payment_ways')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
