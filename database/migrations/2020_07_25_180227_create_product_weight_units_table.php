<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductWeightUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_weight_units', function (Blueprint $table) {
            $table->id();
            $table->float('start_from')->default(1);
            $table->double('price');
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('weight_unit_id')->unsigned();
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('weight_unit_id')->references('id')->on('weight_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_weight_units');
    }
}
