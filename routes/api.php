<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::group(['namespace' => 'API'], function () {

    # ================= GET ROTUES ==================
    Route::get('categories', ['as' => 'get.categories', 'uses' => 'PublicController@categories']);
    Route::get('available-times', ['as' => 'get.availability', 'uses' => 'PublicController@availability']);
    Route::get('get-configs', ['as' => 'get.configs', 'uses' => 'PublicController@configs']);
    Route::get('get-code/{promoCode}', ['as' => 'get.configs', 'uses' => 'PublicController@code']);

    Route::post('products', ['as' => 'get.products', 'uses' => 'PublicController@products']);
    Route::get('products/{product}', ['as' => 'get.productInfo', 'uses' => 'PublicController@productInfo']);

    Route::get('aboutus', ['as' => 'get.aboutus', 'uses' => 'PublicController@getAboutUs']);

    # ================= POST ROTUES =================
    Route::post('login', ['as' => 'login', 'uses' => 'PublicController@login']);
    Route::post('signup', ['as' => 'signup', 'uses' => 'PublicController@signUp']);
    Route::post('contact-us', ['as' => 'contact.us', 'uses' => 'PublicController@sendContactUs']);
    Route::post('forgetPasswordRequest', ['as' => 'forget.password', 'uses' => 'PublicController@forgetPasswordRequest']);
    Route::post('resetPassword', ['as' => 'reset.password', 'uses' => 'PublicController@resetPassword']);

    Route::group(['middleware' => 'auth:api', 'as' => 'user.'], function () {
        Route::get('me', ['as' => 'me', 'uses' => 'UserController@me']);

        Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);

        Route::post('add-to-cart', ['as' => 'add.cart', 'uses' => 'UserController@addToCart']);

        Route::put('edit-profile', ['as' => 'edit.profile', 'uses' => 'UserController@editProfile']);

        Route::get('cart-items',  ['as' => 'cart.items', 'uses' => 'UserController@cartItems']);
        Route::put('edit-cart-item',  ['as' => '', 'uses' => 'UserController@editCartItem']);
        Route::post('delete-cart-item',  ['as' => '', 'uses' => 'UserController@deleteCartItem']);
        Route::post('checkout', ['as' => '', 'uses' => 'UserController@checkout']);
        Route::get('my-orders',    ['as' => '', 'uses' => 'UserController@myOrders']);
        Route::get('my-orders/{id}',    ['as' => '', 'uses' => 'UserController@orderDetails']);
        Route::get('send-receipt/{id}',    ['as' => 'receipt', 'uses' => 'UserController@sendReceipt']);
        //Route::get('deleteOrder', ['as' => '', 'uses' => 'UserController@deleteOrder']);

    });
});
