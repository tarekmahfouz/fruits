<?php


view()->share('resource',url('').'/');

/* Route::group(['prefix' => 'admin-panel', 'middleware' => 'auth:admin'],function(){
	Route::get('/{any?}',['as' => 'home','uses' => 'SystemControllerAjax@home'])->where('any','.*');
});

Route::group(['namespace' => 'Admin', 'as' => 'admin.', 'prefix' => 'admin-api'], function() {
	Route::get('/login',['as' => 'login', 'uses' => 'PublicController@login']);
	Route::post('submit-login',['as' => 'submit.login','uses' => 'PublicController@submitLogin']);
	Route::group(['middleware' => 'auth:admin'],function(){

		Route::get('me', ['as' => 'me', 'uses' => 'AdminControllerAjax@me']);
		
		# ADMINS
		Route::get('get-admins', ['as' => 'get.admins', 'uses' => 'AdminControllerAjax@admins']);
		Route::get('get-admin/{id}', ['as' => 'get.admin', 'uses' => 'AdminControllerAjax@getAdmin']);
		Route::delete('delete-admin/{id}', ['as' => 'delete.admin', 'uses' => 'AdminControllerAjax@deleteAdmin']);
		Route::post('add-admin', ['as' => 'add.admin', 'uses' => 'AdminControllerAjax@addAdmin']);
		Route::put('edit-admin/{id}', ['as' => 'edit.admin', 'uses' => 'AdminControllerAjax@editAdmin']);
		
		# USERS
		Route::get('get-users', ['as' => 'get.users', 'uses' => 'UserControllerAjax@users']);
		Route::get('get-user/{id}', ['as' => 'get.user', 'uses' => 'UserControllerAjax@getUser']);
		Route::delete('delete-user/{id}', ['as' => 'delete.user', 'uses' => 'UserControllerAjax@deleteUser']);
		Route::post('add-user', ['as' => 'add.user', 'uses' => 'UserControllerAjax@addUser']);
		Route::put('edit-user/{id}', ['as' => 'edit.user', 'uses' => 'UserControllerAjax@editUser']);
		
		# USERS
		Route::get('get-categories', ['as' => 'get.categories', 'uses' => 'SystemControllerAjax@categories']);
		Route::delete('delete-category/{id}', ['as' => 'delete.category', 'uses' => 'SystemControllerAjax@deleteCategory']);
		Route::post('add-category', ['as' => 'add.category', 'uses' => 'SystemControllerAjax@addCategory']);
		Route::put('edit-category/{id}', ['as' => 'edit.category', 'uses' => 'SystemControllerAjax@editCategory']);
		
		# CONFIG
		Route::get('get-config', ['as' => 'get.config', 'uses' => 'SystemControllerAjax@getConfig']);
		Route::put('edit-config', ['as' => 'edit.config', 'uses' => 'SystemControllerAjax@editConfig']);
		
		# AVAILABILITIES
		Route::get('get-availabilities', ['as' => 'get.availabilities', 'uses' => 'SystemControllerAjax@availabilities']);
		Route::post('add-availability', ['as' => 'add.availability', 'uses' => 'SystemControllerAjax@addAvailability']);
		Route::put('edit-availability/{id}', ['as' => 'edit.availability', 'uses' => 'SystemControllerAjax@editAvailability']);
		Route::delete('delete-availability/{id}', ['as' => 'delete.availability', 'uses' => 'SystemControllerAjax@deleteAvailability']);
		
		# WEIGHT UNITS
		Route::get('get-weights', ['as' => 'get.weights', 'uses' => 'SystemControllerAjax@weights']);
		Route::post('add-weight', ['as' => 'add.weight', 'uses' => 'SystemControllerAjax@addWeight']);
		Route::put('edit-weight/{id}', ['as' => 'edit.weight', 'uses' => 'SystemControllerAjax@editWeight']);
		Route::delete('delete-weight/{id}', ['as' => 'delete.weight', 'uses' => 'SystemControllerAjax@deleteWeight']);
		
		# INBOX
		Route::get('get-inbox', ['as' => 'get.config', 'uses' => 'SystemControllerAjax@getInbox']);
		Route::delete('delete-message/{id}', ['as' => 'get.config', 'uses' => 'SystemControllerAjax@deleteMessage']);

		# SYSTEM CONTENT
		Route::get('get-content', ['as' => 'get.content', 'uses' => 'SystemControllerAjax@getContent']);
		Route::put('edit-content', ['as' => 'edit.content', 'uses' => 'SystemControllerAjax@editContent']);
		
		# ORDERS
		Route::get('get-orders-count', ['as' => 'get.orders.count', 'uses' => 'ProductControllerAjax@getOrdersCount']);
		Route::get('get-orders', ['as' => 'get.orders', 'uses' => 'ProductControllerAjax@getOrders']);
		Route::get('get-order/{id}', ['as' => 'get.order', 'uses' => 'ProductControllerAjax@getOrder']);
		Route::put('edit-order/{id}', ['as' => 'edit.order', 'uses' => 'ProductControllerAjax@editOrder']);
		
		# PRODUCTS
		Route::get('get-products', ['as' => 'get.products', 'uses' => 'ProductControllerAjax@getProducts']);
		Route::get('get-product/{id}', ['as' => 'get.product', 'uses' => 'ProductControllerAjax@getProduct']);
		Route::post('product/store', ['as' => 'create.product', 'uses' => 'ProductControllerAjax@createProduct']);
		Route::put('edit-product/{id}', ['as' => 'edit.product', 'uses' => 'ProductControllerAjax@editProduct']);
		Route::delete('delete-product/{id}', ['as' => 'delete.product', 'uses' => 'ProductControllerAjax@deleteProduct']);
		Route::delete('delete-product-price/{id}', ['as' => 'delete.product.price', 'uses' => 'ProductControllerAjax@deleteProductPrice']);
		
		Route::get('logout', ['as' => 'logout', 'uses' => 'AdminControllerAjax@logout']);
		
	});
}); */


Route::group(['namespace' => 'Admin', 'as' => 'admin.'], function() {
	Route::get('/login',['as' => 'login', 'uses' => 'PublicController@login']);
	Route::post('submit-login',['as' => 'submit.login','uses' => 'PublicController@submitLogin']);
	Route::group(['middleware' => 'auth:admin'],function(){

		Route::get('me', ['as' => 'me', 'uses' => 'AdminControllerAjax@me']);
		
		# ADMINS
		Route::get('get-admins', ['as' => 'get.admins', 'uses' => 'AdminControllerAjax@admins']);
		Route::get('get-admin/{id}', ['as' => 'get.admin', 'uses' => 'AdminControllerAjax@getAdmin']);
		Route::delete('delete-admin/{id}', ['as' => 'delete.admin', 'uses' => 'AdminControllerAjax@deleteAdmin']);
		Route::post('add-admin', ['as' => 'add.admin', 'uses' => 'AdminControllerAjax@addAdmin']);
		Route::put('edit-admin/{id}', ['as' => 'edit.admin', 'uses' => 'AdminControllerAjax@editAdmin']);
		
		# USERS
		Route::get('get-users', ['as' => 'get.users', 'uses' => 'UserControllerAjax@users']);
		Route::get('get-user/{id}', ['as' => 'get.user', 'uses' => 'UserControllerAjax@getUser']);
		Route::delete('delete-user/{id}', ['as' => 'delete.user', 'uses' => 'UserControllerAjax@deleteUser']);
		Route::post('add-user', ['as' => 'add.user', 'uses' => 'UserControllerAjax@addUser']);
		Route::put('edit-user/{id}', ['as' => 'edit.user', 'uses' => 'UserControllerAjax@editUser']);
		
		# USERS
		Route::get('get-categories', ['as' => 'get.categories', 'uses' => 'SystemControllerAjax@categories']);
		Route::delete('delete-category/{id}', ['as' => 'delete.category', 'uses' => 'SystemControllerAjax@deleteCategory']);
		Route::post('add-category', ['as' => 'add.category', 'uses' => 'SystemControllerAjax@addCategory']);
		Route::put('edit-category/{id}', ['as' => 'edit.category', 'uses' => 'SystemControllerAjax@editCategory']);
		
		# CONFIG
		Route::get('get-config', ['as' => 'get.config', 'uses' => 'SystemControllerAjax@getConfig']);
		Route::put('edit-config', ['as' => 'edit.config', 'uses' => 'SystemControllerAjax@editConfig']);
		
		# AVAILABILITIES
		Route::get('get-availabilities', ['as' => 'get.availabilities', 'uses' => 'SystemControllerAjax@availabilities']);
		Route::post('add-availability', ['as' => 'add.availability', 'uses' => 'SystemControllerAjax@addAvailability']);
		Route::put('edit-availability/{id}', ['as' => 'edit.availability', 'uses' => 'SystemControllerAjax@editAvailability']);
		Route::delete('delete-availability/{id}', ['as' => 'delete.availability', 'uses' => 'SystemControllerAjax@deleteAvailability']);
		
		# WEIGHT UNITS
		Route::get('get-weights', ['as' => 'get.weights', 'uses' => 'SystemControllerAjax@weights']);
		Route::post('add-weight', ['as' => 'add.weight', 'uses' => 'SystemControllerAjax@addWeight']);
		Route::put('edit-weight/{id}', ['as' => 'edit.weight', 'uses' => 'SystemControllerAjax@editWeight']);
		Route::delete('delete-weight/{id}', ['as' => 'delete.weight', 'uses' => 'SystemControllerAjax@deleteWeight']);
		
		# INBOX
		Route::get('get-inbox', ['as' => 'get.config', 'uses' => 'SystemControllerAjax@getInbox']);
		Route::delete('delete-message/{id}', ['as' => 'get.config', 'uses' => 'SystemControllerAjax@deleteMessage']);

		# SYSTEM CONTENT
		Route::get('get-content', ['as' => 'get.content', 'uses' => 'SystemControllerAjax@getContent']);
		Route::put('edit-content', ['as' => 'edit.content', 'uses' => 'SystemControllerAjax@editContent']);
		
		# PROMO CODES
		Route::get('get-codes', ['as' => 'get.codes', 'uses' => 'SystemControllerAjax@codes']);
		Route::delete('delete-code/{id}', ['as' => 'delete.code', 'uses' => 'SystemControllerAjax@deleteCode']);
		Route::post('add-code', ['as' => 'add.code', 'uses' => 'SystemControllerAjax@addCode']);
		Route::put('edit-code/{id}', ['as' => 'edit.code', 'uses' => 'SystemControllerAjax@editCode']);
		Route::post('send-notifications', ['as' => 'send.notifications', 'uses' => 'UserControllerAjax@sendNotifications']);	

		# ORDERS
		Route::get('get-orders-count', ['as' => 'get.orders.count', 'uses' => 'ProductControllerAjax@getOrdersCount']);
		Route::get('get-orders', ['as' => 'get.orders', 'uses' => 'ProductControllerAjax@getOrders']);
		Route::get('get-order/{id}', ['as' => 'get.order', 'uses' => 'ProductControllerAjax@getOrder']);
		Route::put('edit-order/{id}', ['as' => 'edit.order', 'uses' => 'ProductControllerAjax@editOrder']);
		
		# PRODUCTS
		Route::get('get-products', ['as' => 'get.products', 'uses' => 'ProductControllerAjax@getProducts']);
		Route::get('get-product/{id}', ['as' => 'get.product', 'uses' => 'ProductControllerAjax@getProduct']);
		Route::post('add-product', ['as' => 'create.product', 'uses' => 'ProductControllerAjax@createProduct']);
		Route::put('edit-product/{id}', ['as' => 'edit.product', 'uses' => 'ProductControllerAjax@editProduct']);
		Route::delete('delete-product/{id}', ['as' => 'delete.product', 'uses' => 'ProductControllerAjax@deleteProduct']);
		Route::delete('delete-product-price/{id}', ['as' => 'delete.product.price', 'uses' => 'ProductControllerAjax@deleteProductPrice']);
		
		Route::get('logout', ['as' => 'logout', 'uses' => 'AdminControllerAjax@logout']);
		

		Route::get('/{any?}',['as' => 'home','uses' => 'SystemControllerAjax@home'])->where('any','.*');
	});
});